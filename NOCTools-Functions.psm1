<#DNS Functions#>
function Find-DNSRecord {
    param ($Search)
    while (!$Search) {$Search = Read-Host -Prompt "`nWhat domain name or IP are you looking for?`n"}
    Resolve-DnsName -server $($DNSControllers[0].Hostname) -name "$Search"
}
Function Get-DNSAliasesForCNAME {
    param ($DomainObj)
    $Aliases = Get-DnsServerResourceRecord -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($Domain.TopLevelDomain)" -RRType "CNAME" | Where-Object {$_.RecordData.HostnameAlias -eq "$($Domain.FQDN)."}
    Return $Aliases
}
Function Get-DNSRecordData {
    param ($Domain)
    If ($Domain){
        $DomainData = Get-DnsServerResourceRecord -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($Domain.TopLevelDomain)" -Name "$($Domain.Subdomain)" -ErrorAction SilentlyContinue | Where-Object -Property "Hostname" -EQ "$($Domain.Subdomain)"
        If ($DomainData){Return $DomainData[0]}
    }
    Return $false
}
Function Get-DNSRecordDestination {
    param (
        $Domain,
        $DomainType
    )
    If ($DomainType -eq "A"){
        $DomainDestination = $($Domain.RecordData.IPv4Address.IPAddressToString)
        Return $DomainDestination
    }
    If ($DomainType -eq "AAAA"){
        $DomainDestination = $($Domain.RecordData.IPv6Address.IPAddressToString)
        Return $DomainDestination
    }
    If ($DomainType -eq "CNAME"){
        $DomainDestination = $($Domain.RecordData.HostNameAlias)
        Return $DomainDestination
    }
    Return $false
}
Function Get-DNSRecordPresent {
    param (
        $Domain
    )
    If (Get-DnsServerResourceRecord -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($Domain.ToplevelDomain)" -name "$($Domain.Subdomain)" -ErrorAction SilentlyContinue) {
        Return $True
    }
    Return $False
}
<#================================================================================================================#>
<#DHCP Functions#>
Function Format-IPv4Address {
    param ($IP)
    $IP = $IP -replace "[^0-9\.]", ""
    Return $IP
}
Function Format-IPv6Address {
    param ($IP)
    $IP = $IP -replace "[^a-fA-F0-9\:]", ""
    Return $IP
}
Function Confirm-IPv4Address {
    param ([string]$IP)
    $IP = Format-IPv4address -IP $IP
    If ($IP -Match '^(\b(24[0-5]|2[0-4]\d|[1]\d{2}|[1-9]\d|\d)(\.(25[0-5]|[2][0-4]\d|[1]\d{2}|[1-9]\d|\d)){3}\b)$') {
        $IPout = [PSCustomObject] @{
            IsValid = $TRUE;
            IP = $IP
        }
        Return $IPout
    }
    Return $False
}
Function Confirm-IPv6Address {
    param ([string]$IP)
    $IP = Format-IPv6address -IP $IP
    If ($IP -Match '^(?:(?:([a-fA-F-1-9]{1,4}\:){3}|([a-fA-F0-9]{1,4}\:){2}\:|([a-fA-F0-9]{1,4}\:){1}\:)(?:([a-fA-F0-9]{1,4}){1}))$'){
        $IPout = [PSCustomObject] @{
            IsValid = $TRUE;
            IP = $IP
        }
        Return $IPout
    }
    Return $False
}
Function Confirm-ValidIP {
    param ($IPAddress)
    If ($(Confirm-IPv4Address -IP $IPAddress) -or $(Confirm-IPv6Address -IP $IPAddress)){
        Return $true
    }
    Return $false
}
Function Format-WebDomain {
    param ($Domain)
    $Domain = $Domain -replace "[^a-zA-A0-9\-\.]", ""    
    Return $Domain
}
Function Confirm-WebDomain {
    param ([string]$Domain)
    $Domain = Format-WebDomain -Domain $Domain
    If ($Domain -Match '^(([a-zA-Z0-9\-\.]*)((?:\.[a-zA-Z0-9\-]+?)(?:\.[a-zA-Z]+?)(?:$|\/)))$') {
        $DomainOut = [PSCustomObject] @{
            IsValid = $TRUE;
            FQDN = $Domain
        }
        Return $DomainOut
    }
    Return $False
}
Function Get-Subdomain {
    param ([string]$Domain)
    $Domain = Format-WebDomain -Domain $Domain
    $Subdomain = $Domain | Select-String -Pattern "^([\-\w.]*)(?=\.((.*)\.([a-zA-z]*)))" | ForEach-Object { $_.Matches.Value }
    Return $Subdomain
}
Function Get-TopLevelDomain {
    param ([string]$Domain)
    $Domain = Format-WebDomain -Domain $Domain
    $TLD = $Domain | Select-String -Pattern "((?:\w+?)(?:\.[a-zA-Z]+?)(?:$|\/))$"| ForEach-Object { $_.Matches.Value }
    $TLDout = [PSCustomObject] @{
        IsValid = $TRUE;
        TLD = $TLD
    }
    If ($Domains.TLD -contains $TLD){Return ($TLDout)}
    $TLDout.IsValid = $False
    Return $TLDout 
}
Function Get-ICMPStatus {
    param ([string]$Destination)
    If ($(Test-Connection -count 2 -Delay 1 -ErrorAction SilentlyContinue -ComputerName $Destination)){Return $True}
    Return $False
}
Function Get-IPStatus {
    param ($IP,$IPversion)
    If ($IPversion -eq "IPv4" -and $(Confirm-IPv4Address -IP $IP) -and $(Get-DhcpServerv4Lease -ComputerName $($DHCPControllers[0].Hostname) -IPAddress $IP -ErrorAction SilentlyContinue) ){
        Return $True
    }
    If ($IPversion -eq "IPv6" -and $(Confirm-IPv6Address -IP $IP) -and $(Get-DhcpServerv6Lease -ComputerName $($DHCPControllers[0].Hostname) -IPAddress $IP -ErrorAction SilentlyContinue) ){
        Return $True
    }
    Return $False
}
Function Get-IPData {
    Param ($IP)
    $IPAddress = Get-DhcpServerv4Lease -ComputerName $($DHCPControllers[0].Hostname) -IPAddress $IP -ErrorAction SilentlyContinue
    Return $IPAddress
}
Function Get-ReservationStatus {
    param ($IP)
    If ($IP.AddressState -match "(.*)Reservation"){
        Return $True
    }
    Return $False 
}
Function Get-ScopeData {
    param ($ScopeID,$ScopeType)
    If ($ScopeType -eq "IPv4"){
        $ScopeDetails = Get-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) -scopeid $ScopeID -errorAction SilentlyContinue
        $ScopeStats = Get-DhcpServerv4ScopeStatistics -ComputerName $($DHCPControllers[0].Hostname) -scopeid $ScopeID
        $ScopeDNS = Get-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $ScopeID
        If ($ScopeDetails){ Return ($ScopeDetails,$ScopeStats,$ScopeDNS) }
    }
    If ($ScopeType -eq "IPv6"){}
}
Function Get-ScopeStatus {
    param ($ScopeID,$ScopeType)
    If ($ScopeType -eq "IPv4" -and $(Get-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) -scopeid $ScopeID -errorAction SilentlyContinue)){
        Return $True
    }
    Return $False
}
Function Get-ScopeGateway {
    param ($ScopeID)
    $Gateway = $(Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $ScopeID -OptionId 3 ) 
    Return $Gateway.Value
}
Function Get-SuperScopeName {
    param ($ScopeID)
    $SuperscopeDetails = Get-DhcpServerv4ScopeStatistics -ComputerName $($DHCPControllers[0].Hostname) -scopeid $ScopeID |Select-Object -Property SuperscopeName
    If ($SuperscopeDetails){Return $SuperscopeDetails.SuperscopeName}
}
Function Get-SuperScopeSubScopes {
    param ($SuperscopeName)
    If ($SuperscopeName){
        $SuperscopeDetails = Get-DhcpServerv4Superscope -ComputerName $($DHCPControllers[0].Hostname) | Where-Object -Property SuperscopeName -Match "$($SuperscopeName)"
        Return $SuperscopeDetails.ScopeId
    }
}
Function Get-ScopeOptions {
    param ($ScopeID)
    $ScopeOptions = $(Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $ScopeID) | Select-Object Name,OptionId,Value
    Return $ScopeOptions
}
Function Get-ScopeLeases {
    param ($Scope,$ScopeType)
    If ($ScopeType -eq "IPv4"){
        $ScopeLeases = $(Get-DhcpServerv4Lease -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $Scope -AllLeases)
    }
    Return $ScopeLeases
}
Function Get-IPReservationDescription {
    param ($IP,$IPType)
    If ($IPType -eq "IPv4"){
        $ReservedIP = Get-DhcpServerv4Reservation -ComputerName $($DHCPControllers[0].Hostname) -IPAddress $IP
    }
    Return $ReservedIP.Description
}
Function Confirm-IPinScopeRange {
    param ($IP,$Scope)
    $ScopeRange = Get-NetworkRange "$($Scope.ScopeID)/$($Scope.ScopeNetMask)"
    If ($($ScopeRange.IPAddressToString) -Contains $IP.IP) {Return $True}
    Return $False
}
<#================================================================================================================#>
<#RADIUS Functions#>
Function Get-RADIUSServerStatus {
    $SessionCheck = Get-PSSession
    If ($SessionCheck){Return}
    Connect-RADIUSServers
}
<#================================================================================================================#>
<#SSH Functions#>
<# 
Using SSH with power shell requires the Posh-SSH installed
These can be installed using built in powershell commands from an admin instance of power shell
> Get-Module Posh-SSH
> Install-Module Posh-SSH
#>
Function Get-SSHCoreRouterConnectionStatus {
    If ($CoreConnections){Return}
    Write-Host -ForegroundColor $Colors.Warn "`nNo SSH Connections found. Opening connections" 
    While (!$CoreConnections){Open-SSHCoreConnections}
    Return
}
<#================================================================================================================#>
<#Misc Functions#>
Function New-NetworkTrunksWin10 {
    If(!$(Test-IsAdmin)) {
        Write-Host -ForegroundColor $Colors.Warn "Function requires Admin privileges."
        Break
    }
    Write-Host "`nPlease ensure that you have Hyper-V enabled and have created a new external switch in Hyper-V manager"
    #Write-Host "`n--------------------`nThe following will list the currently available Hyper-V switches.`n--------------------`n"
    <# Request required variables #>
    $switch = Read-Host "Please enter the name of your newly created Hyper-V external switch"
    $ifid = Read-Host "Please enter the name desired for the new interface (v10-Production)"
    $vlanid = Read-Host "Please enter the VLAN ID for the new interface (10)"
    Add-VMNetworkAdapter -ManagementOS -Name $ifid -SwitchName $switch
    Set-VMNetworkAdapterVlan -ManagementOS -VMNetworkAdapterName $ifid -Access -VlanId $vlanid
}
Function Test-IsAdmin {
    try {
        $identity = [Security.Principal.WindowsIdentity]::GetCurrent()
        $principal = New-Object Security.Principal.WindowsPrincipal -ArgumentList $identity
        return $principal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator )
    } 
    catch {throw "Failed to determine if the current user has elevated privileges. The error was: '{0}'." -f $_}

    <#
        .SYNOPSIS
            Checks if the current Powershell instance is running with elevated privileges or not.
        .EXAMPLE
            PS C:\> Test-IsAdmin
        .OUTPUTS
            System.Boolean
                True if the current Powershell is elevated, false if not.
    #>
}
Function Get-ShellHistory {Get-Content (Get-PSReadlineOption).HistorySavePath}
Function Get-FileName($initialDirectory){  
 [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
 $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
 $OpenFileDialog.initialDirectory = $initialDirectory
 $OpenFileDialog.filter = "All files (*.*)| *.*"
 $OpenFileDialog.ShowDialog() | Out-Null
 $OpenFileDialog.filename
}
Function ChangeLog {. O:\Utils\ChangeLog\AddChangeLogEntryForm.exe}
Function Set-NetUserPassword {Set-ADAccountPassword -Identity "net_$($env:UserName)" -OldPassword $(Read-Host -Prompt "`nEnter Old Password" -AsSecureString) -NewPassword $(Read-Host -Prompt "`nEnter New Password" -AsSecureString)}
Function Format-TextToBase64 {
    param ([string] $Text)
    $Bytes = [System.Text.Encoding]::Unicode.GetBytes($Text)
    $EncodedText =[Convert]::ToBase64String($Bytes)
    $EncodedText
}
Function Format-Base64ToText {
    param ([string] $EncodedText)
    $DecodedText = [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($EncodedText))
    $DecodedText
}
Function Convert-JuniperXMLtoObject {
    param ( $XMLin )
    If (!$XMLin){Return $False}
    $XMLout = Select-Xml -Content $([string]$XMLin.Output) -XPath "/rpc-reply"
    Return $XMLout
}
Function Add-FullWidthLine {
    param ($Color,$LineCharacter)
    If (!$Color){$Color = "$($Colors.Good)"}
    If (!$LineCharacter) {$LineCharacter = "="}
    Write-Host -ForegroundColor $Color "`n$($Linecharacter * (Get-Host).UI.RawUI.WindowSize.Width)`n"
}