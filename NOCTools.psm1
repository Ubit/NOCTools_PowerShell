<#
Written by: Steven Malone
#>

<# This function collects all of the smaller functions into an easy to use manner with TAB completeable options and help options#>

Class NetworkSwitch {
    NetworkSwitch(){}
    [string] $Name;
    [ValidateSet ("Cisco","Extreme","Juniper","3Com","Dell")] $Manufacturer;
    [string] $Model;
    [string] $Ports;
    [ValidateSet ("Telnet","SSH")] $AccessMethod;
    [string] $Template;
    [string] $VLANConfig;
    [string] $PortConfig;
    [void] AddSwitch([string] $Switch){
        $this.Name = $Switch
    }
}
Class Domain {
    Domain(){}
    [string] $FQDN;
    [string] $TopLevelDomain;
    [boolean] $SystemTLD;
    [string] $Subdomain;
    [array] $Aliases;
    [string] $Destination;
    [boolean] $IsPresent;
    [string] $RecordType
    [void] AddDomain([string] $Domain) {
        $DomainOut = Confirm-WebDomain -Domain $Domain
        If ($DomainOut.IsValid)  {
            $this.FQDN = $DomainOut.FQDN
            $this.Subdomain = $(Get-Subdomain -Domain $this.FQDN)
            $TLD = $(Get-TopLevelDomain -Domain $this.FQDN)
            $this.TopLevelDomain = $($TLD.TLD)
            $this.SystemTLD = $TLD.IsValid
            If ($this.SystemTLD){
                $this.IsPresent = $(Get-DNSRecordPresent -Domain $this)
                If ($this.IsPresent){
                    $DomainData = Get-DNSRecordData -Domain $this
                    $this.RecordType = $DomainData.RecordType
                    $this.Aliases = Get-DNSAliasesForCNAME -DomainObj $this
                    $this.Destination = Get-DNSRecordDestination -Domain $DomainData -DomainType $this.RecordType
                }
            }
        }
    }
}
Class ScopeData {
    ScopeData(){}
    [string] $ScopeID;
    [ValidateSet ("IPv4","IPv6")] $ScopeType;
    [boolean] $ScopeIsPresent;
    [string] $Gateway;
    [string] $ScopeName;
    [string] $ScopeNetMask;
    [string] $StartRange;
    [string] $EndRange;
    [string] $ScopeState;
    [string] $LeaseDuration;
    [string] $SuperscopeName;
    [string] $SSIncludedScopes;
    [array] $ScopeOptions;
    [array] $ScopeLeases;
    [string] $AvailableAddresses;
    [string] $AddressesInUse;
    [string] $PercentageInUse;
    [string] $AddressesReserved;
    [string] $DynamicDNSUpdates;
    [boolean] $DNSNameProtection;
    [boolean] $DeleteDNSRROnLeaseExpiry;
    [boolean] $DisableDnsPtrRRUpdate;

    [void] AddScope([string] $Scope) {
        If ($(Confirm-IPv4Address -IP $Scope)){
            $this.ScopeID = $Scope;
            $this.ScopeType = "IPv4"
        }
        If ($(Confirm-IPv6Address -IP $Scope)){
            $this.ScopeID = $Scope;
            $this.ScopeType = "IPv6"
        }
        ElseIF (!$this.ScopeID) {
            Throw "$Scope is not a valid Scope ID."
        }

        $this.ScopeIsPresent = $(Get-ScopeStatus -ScopeID $this.ScopeID -ScopeType $this.ScopeType)
        If ($this.ScopeIsPresent){
            $ScopeData = $(Get-ScopeData -ScopeID $this.ScopeID -ScopeType $this.ScopeType)
            $this.ScopeIsPresent = $True
            $this.ScopeName = $ScopeData[0].Name
            $this.ScopeNetMask = $ScopeData[0].SubnetMask
            $this.Gateway = $(Get-ScopeGateway -ScopeID $this.ScopeID)
            $this.StartRange = $ScopeData[0].StartRange.IPAddressToString
            $this.EndRange = $ScopeData[0].EndRange.IPAddressToString
            $this.ScopeState = $ScopeData[0].State
            $this.LeaseDuration = $ScopeData[0].LeaseDuration.ToString()
            $this.SuperScopeName = $(Get-SuperScopeName -ScopeID $this.ScopeID)
            If ($this.SuperScopeName){
                $this.SSIncludedScopes = $(Get-SuperScopeSubScopes -SuperscopeName $this.SuperScopeName)
            }
            $this.ScopeOptions = $(Get-ScopeOptions -ScopeID $this.ScopeID)
            $this.ScopeLeases = $(Get-ScopeLeases -Scope $this.ScopeID -ScopeType $this.ScopeType)
            $this.AvailableAddresses = $ScopeData[1].AddressesFree
            $this.AddressesInUse = $ScopeData[1].AddressesInUse
            $this.PercentageInUse = $ScopeData[1].PercentageInUse
            $this.AddressesReserved = $ScopeData[1].ReservedAddress
            $this.DynamicDNSUpdates = $ScopeData[2].DynamicUpdates
            $this.DNSNameProtection = $ScopeData[2].NameProtection
            $this.DeleteDNSRROnLeaseExpiry = $Scopedata[2].DeleteDnsRROnLeaseExpiry
            $this.DisableDnsPtrRRUpdate = $ScopeData[2].DisableDnsPtrRRUpdate
        }
    }
}
Class IPData {
    IPData(){}
    [string] $IP;
    [ValidateSet ("IPv4","IPv6")] $IPType;
    [boolean] $LeaseIsPresent;
    [boolean] $IsReserved;
    [string] $Description;
    [string] $MAC;
    [string] $Hostname;
    [string] $LeaseStatus;
    [string] $ScopeName;
    [string] $Gateway;
    [string] $ScopeID;
    [string] $ScopeCIDR;
    [string] $SuperscopeName;
    [String] $SSIncludedScopes;
    [void] AddIP([string] $IP) {
        $IP4out = $(Confirm-IPv4Address -IP $IP)
        If ($IP4out.IsValid){
            $this.IP = $IP4out.IP;
            $this.IPType = "IPv4"
        }
        $IP6out = $(Confirm-IPv6Address -IP $IP)
        If ($IP6out){
            $this.IP = $IP6out.IP;
            $this.IPType = "IPv6"
        }
        ElseIF (!$this.IP) {
            Throw "$IP is not a valid IP."
        }
        $this.LeaseIsPresent = $(Get-IPStatus -IP $this.IP -IPversion $this.IPType)
        If ($this.LeaseIsPresent){
            $IPAddress = $(Get-IPData -IP $this.IP)
            $this.Hostname = $IPaddress.Hostname
            $this.MAC = $IPaddress.ClientID
            If ($this.IsReserved){
                $this.MAC = $(Get-ReservationDescription)
            }
            $this.ScopeID = $IPaddress.ScopeId
            $this.IsReserved = $(Get-ReservationStatus -IP $IPAddress)
            If ($this.IsReserved){
                $this.Description = $(Get-IPReservationDescription -IP $this.IP -IPType $this.IPType)
            }
            $this.LeaseStatus = $IPAddress.AddressState
            $ScopeData = $(Get-ScopeData -ScopeID $this.ScopeID -ScopeType $this.IPType)
            $this.ScopeName = $ScopeData.Name
            $this.ScopeCIDR = $ScopeData.SubnetMask
            $this.Gateway = $(Get-ScopeGateway -ScopeID $this.ScopeID)
            $this.SuperScopeName = $(Get-SuperScopeName -ScopeID $this.ScopeID)
            If ($this.SuperScopeName){
                $this.SSIncludedScopes = $(Get-SuperScopeSubScopes -SuperscopeName $this.SuperScopeName)
            }
        }
    }
}
$global:DefaultVLANs = @(
    [PSCustomObject] @{
    Name = '1st Floor Production';
    ID = '101';
    },
    [PSCustomObject] @{
    Name = '2nd Floor Production';
    ID = '102';
    },
    [PSCustomObject] @{
    Name = '3rd Floor Production';
    ID = '103';
    },
    [PSCustomObject] @{
    Name = '4th Floor Production';
    ID = '104';
    },
    [PSCustomObject] @{
    Name = 'Basement Floor Production';
    ID = '191';
    },
    [PSCustomObject] @{
    Name = 'Wireless';
    ID = '525';
    },
    [PSCustomObject] @{
    Name = 'Guest Wired';
    ID = '766';
    },
    [PSCustomObject] @{
    Name = 'POS';
    ID = '901';
    },
    [PSCustomObject] @{
    Name = 'UPD';
    ID = '1076';
    },
    [PSCustomObject] @{
    Name = 'Building Control';
    ID = '1077';
    },
    [PSCustomObject] @{
    Name = 'AV-Production';
    ID = '1080';
    },
    [PSCustomObject] @{
    Name = 'Isolated';
    ID = '1085';
    },
    [PSCustomObject] @{
    Name = 'POS';
    ID = '1091';
    },
    [PSCustomObject] @{
    Name = 'Management';
    ID = '1500';
    },
    [PSCustomObject] @{
    Name = 'Production Wireless';
    ID = '2001';
    },
    [PSCustomObject] @{
    Name = 'Guest Wireless';
    ID = '2100';
    },
    [PSCustomObject] @{
    Name = 'Registration';
    ID = '2200';
    },
    [PSCustomObject] @{
    Name = 'Remediation';
    ID = '2500';
    }
)
$global:DHCPControllers = @(
    [PSCustomObject] @{
    Name = 'Hostname';
    Hostname = 'FQDN';
    },
    [PSCustomObject] @{
    Name = 'Hostname2';
    Hostname = 'FQDN2';
    }
)
$global:DNSControllers = @(
    [PSCustomObject] @{
    Name = 'Hostname';
    Hostname = 'FQDN';
    IP = 'IP';
    PrimaryDomainFile = '\\path\to\primary\domain\file.dns';
    SecondaryDomainFile = '\\path\to\secondary\domain\file.dns';
    ReverseLookupFile = '\\path\to\reverse\lookup\file.dns';
    }
)
$global:RADIUSServers = @(
    [PSCustomObject] @{
    Name = 'Hostname';
    Hostname = 'FQDN';
    },
    [PSCustomObject] @{
    Name = 'Hostname2';
    Hostname = 'FQDN2';
    },
)
$global:CoreRouters = @(
    [PSCustomObject] @{
    Name = 'Friendly_Name';
    FQDN = 'FQDN';
    IP = 'IP';
    Type = 'Border/Core/Remote/Etc';
    },
    [PSCustomObject] @{
    Name = 'Friendly_Name2';
    FQDN = 'FQDN2';
    IP = 'IP2';
    Type = 'Border/Core/Remote/Etc';
    }
)
$global:SNMPStrings = @(
    "String1","String2"
)
$global:Colors = @(
    [PSCustomObject] @{
    # These can be changed to user color preferences
    Good = "Green"; 
    Info = "Yellow"; 
    Warn = "Red"; 
    BackgroundColor = "Black";
    OriginalBackground = "DarkMagenta"; 
    }
)
$global:Domains = @(
    [PSCustomObject]@{
        TLD = 'domain.example';
    }
    [PSCustomObject]@{
        TLD = 'secondarydomain.example';
    }
)
$global:IPSpace = @(
    [PSCustomObject]@{
        Scope = 'IP_Octets_Of_Public_Scope(111.222)'
        Public = $true
    }
    [PSCustomObject]@{
        Scope = 'IP_Octets_Of_Public_Scope2'
        Public = $true
    }
)
$global:SubModules = @(
    [PSCustomObject] @{
        ModuleName = 'NOCTools-DHCP';
        ModuleCommand = "Get-IPInfo";
    },
    [PSCustomObject] @{
        ModuleName = 'NOCTools-DNS';
        ModuleCommand = "Get-DNSRecordData";
    },
    [PSCustomObject] @{
        ModuleName = 'NOCTools-SSH';
        ModuleCommand = "Get-SSHCoreRouterConnectionStatus";
    },
    [PSCustomObject] @{
        ModuleName = 'NOCTools-RADIUS';
        ModuleCommand = "Get-RADIUSServerStatus";
    },
    [PSCustomObject] @{
        ModuleName = 'NOCTools-Other';
        ModuleCommand = "Zlookup";
    }
)
$global:ExternalModules = @(
    [PSCustomObject] @{ 
        ModuleName = "Indented.Net.IP"
    },
    [PSCustomObject] @{ 
        ModuleName = "ImportExcel"
    },
    [PSCustomObject] @{ 
        ModuleName = "Posh-SSH"
    },
    [PSCustomObject] @{ 
        ModuleName = "DhcpServer"
    },
    [PSCustomObject] @{ 
        ModuleName = "DnsServer"
    }
)
$ConfigTemplates = "O:\NOC\CONFIG TEMPLATES\"
$ExtremeX440 = [NetworkSwitch]::New()
$ExtremeX440.Manufacturer = "Extreme"
$ExtremeX440.Model = "X440" 
$ExtremeX440.Ports = "52" 
$ExtremeX440.AccessMethod = "SSH" 
$ExtremeX440.Template = $(Get-Content "$($ConfigTemplates)Extreme\X440_baseconfig.txt" -Raw)
$ExtremeX440.VLANConfig = '
create vlan <VLANName> tag <VLANID> 
configure vlan <VLANName> description "<VLANDescription>" 
configure vlan <VLANName> add ports 52 tagged '
$ExtremeX440.PortConfig = '
configure vlan <VLANName> add ports <PortsList> untagged
enable ports <PortsList>'

<##############
NOCTools Function 
##############>

function NOCTools {
    param (
        [ValidateSet(
            "Create New IPv4 Scope","IP Info","Scope Info","Create DHCP Reservation","Modify DHCP Reservation",
            "Delete DHCP Reservation","Next IP in Scope","List Leases","Search Primary Logs",
            "Search Secondary Logs","Find MAC","Search Scope Names or ID",
            "Add Aruba Options","Add Dell Options","Create Group of New Scopes from Excel","Delete DHCP Scope"
        )] $DHCP, 
        [ValidateSet(
            "Create DNS Record","Delete DNS Record",
            "Modify DNS Record","Search","ZLookup"
        )] $DNS,
        [ValidateSet(
            "Get Core Logs","Get Core Alarms",
            "Get Core Down Interfaces","Search Cores for MAC or IP",
            "Get Core Interace Bandwidth Usage"
        )] $SSH,
        [ValidateSet(
            "Switch Config Template"
        )] $Configure,
        [ValidateSet(
            "New RADIUS Entry"
        )] $RADIUS
    )
    $Host.UI.RawUI.BackgroundColor = $Colors.BackgroundColor
    If ($DHCP){
        <# Scope Details and Information #>
        if($DHCP -eq 'Search Scope Names or ID') {Find-DHCPScopeNameorID}
        If ($DHCP -eq 'IP Info'){Get-IPInfo}
        If ($DHCP -eq 'Scope Info'){Get-ScopeInfo }
        If ($DHCP -eq "Next IP in Scope"){Find-DHCPScopeNextIP}
        If ($DHCP -eq "Find MAC"){Search-DHCPMAC}
        If ($DHCP -eq "List Leases"){Get-DHCPScopeLeases }
        If ($DHCP -eq "Create DHCP Reservation"){New-DHCPIPReservation }
        If ($DHCP -eq "Modify DHCP Reservation"){Edit-DHCPExistingReservation}
        If ($DHCP -eq "Delete DHCP Reservation"){Remove-DHCPIPRecord}
        If ($DHCP -eq "Create New IPv4 Scope"){New-DHCPScope}
        If ($DHCP -eq "Create Group of New Scopes from Excel"){New-DHCPMultiScope}
        If ($DHCP -eq "Delete DHCP Scope"){Remove-DHCPScope}
        If ($DHCP -eq "Add Aruba Options"){Set-DHCPScopeArubaOptions }
        If ($DHCP -eq "Add Dell Options"){Set-DHCPScopeDellOptions}
        If ($DHCP -eq "Search Primary Logs") {Search-DHCPLogs -Server "Primary"}
        If ($DHCP -eq "Search Secondary Logs") {Search-DHCPLogs -Server "Secondary"}
    }
    If ($DNS){
        If ($DNS -eq 'Search') {Find-DNSRecord}
        If ($DNS -eq 'ZLookup') {zlookup}
        If ($DNS -eq 'Create DNS Record' -or $DNS -eq 'Modify DNS Record') {New-DNSRecord}
        If ($DNS -eq 'Delete DNS Record'){Delete-DNSRecord}
    }
    If ($SSH){
        If ( $SSH -ieq "Get Core Logs" ) {Get-SSHRouterCoreLogs }
        If ( $SSH -ieq "Get Core Alarms" ) {Get-SSHRouterCoreAlarms}
        If ( $SSH -ieq "Get Core Down Interfaces" ) {Get-SSHRouterCoreDownInterfaces}
        If ( $SSH -ieq "Search Cores for MAC or IP" ) {Find-SSHMACinCores}
        If ( $SSH -ieq "Get Core Interace Bandwidth Usage" ) {Get-SSHRouterBandwidth}
    }
    If ($Configure){
        If ($Configure -eq "Switch Config Template"){New-SwitchTemplate}
    }
    If ($RADIUS){
        If ($RADIUS -EQ "New RADIUS Entry"){New-RADIUSEntry}
    }
    $Host.UI.RawUI.BackgroundColor = $Colors.OriginalBackground
}


Function New-SwitchObject {
    param ($Switch)
    If (!$Switch){Return $False}
    $NewSwitch = New-Object -TypeName NetworkSwitch
    $NewSwitch.AddSwitch("$($Switch)")
    Return $NewSwitch
}
Function New-IPObject {
    param ( $IPaddress )
    If (!$IPaddress){Return $False}
    $IP = New-Object -TypeName IPData
    $IP.AddIP("$($IPAddress)")
    Return $IP
}
Function New-ScopeObject{
    param ( $ScopeID )
    If (!$ScopeID){Return $False}
    $Scope = New-Object -TypeName ScopeData
    $Scope.AddScope($ScopeID)
    Return $Scope
}
Function New-DomainObject {
    param ( $FQDN )
    If (!$FQDN) {Return $false}
    $Domain = [Domain]::new();
    $Domain.AddDomain($FQDN)
    Return $Domain
}
#WSL command shortcuts
function dig ($1){wsl dig $1}
function fping ($0,$1,$2,$3,$4){wsl fping $0 $1 $2 $3 $4}
function noping ($1,$2,$3,$4,$5,$6){wsl noping $1 $2 $3 $4 $5 $6}

Set-PSReadLineOption -EditMode vi
