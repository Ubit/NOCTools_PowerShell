# NOCTools-PS

## Basic Usage

NOCTools is a group of PowerShell functions that simplify working with
Windows DHCP and DNS servers via the CLI. The main `NOCTools`
application groups together several modules that each have their own set
of functions.

    NOCTools -<MODULE> <FUNCTION>

    EXAMPLE:
    NOCTools -DHCP "IP Info"

'Confirm-NOCToolsModules' will verify that the required Modules are
present.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="icon"><div class="title">
Note
</div></td>
<td class="content">Some assumptions made by this application. That you already have the necessary tools to manage DNS and DHCP from PowerShell and that you have the access to use those tools. Such as Get-DhcpServerv4Lease.</td>
</tr>
</tbody>
</table>

------------------------------------------------------------------------

## Current list of modules and functions

**DHCP Module**
<details>
    <summary>Details</summary>

<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th class="tableblock halign-left valign-top">Function</th>
<th class="tableblock halign-left valign-top">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Create New IPv4 Scope</p></td>
<td class="tableblock halign-left valign-top"><p>Walks you through creating new DHCP scope</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>IP Info</p></td>
<td class="tableblock halign-left valign-top"><p>Retrieves active IP information from the server</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Scope Info</p></td>
<td class="tableblock halign-left valign-top"><p>Provides details about the specified scope</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Create DHCP Reservation</p></td>
<td class="tableblock halign-left valign-top"><p>Creates a DHCP reservation from scratch or using an existing lease</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Delete DHCP Reservation</p></td>
<td class="tableblock halign-left valign-top"><p>Deletes a IP reservation from the server</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Next IP in Scope</p></td>
<td class="tableblock halign-left valign-top"><p>Provides you with a list of the next available IPs in a scope</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>List Leases</p></td>
<td class="tableblock halign-left valign-top"><p>Lists out the leases for a scope</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Search Primary Logs</p></td>
<td class="tableblock halign-left valign-top"><p>Scans the primary DHCP server’s logs for the specified parameter</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Search Secondary Logs</p></td>
<td class="tableblock halign-left valign-top"><p>Scans the secondary DHCP server’s logs for the specified parameter</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Find MAC</p></td>
<td class="tableblock halign-left valign-top"><p>Searches the DHCP server for the provided MAC</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Search Scope Names</p></td>
<td class="tableblock halign-left valign-top"><p>Searches the DHCP server scopes by name</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Search ScopeID</p></td>
<td class="tableblock halign-left valign-top"><p>Search for scopes by partial ID 192.168.1 instead of 192.168.1.0</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Add Aruba Options</p></td>
<td class="tableblock halign-left valign-top"><p>Adds DHCP options to individual reserved IPs</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Add Dell Options</p></td>
<td class="tableblock halign-left valign-top"><p>Adds DHCP options to the scope that allow Dell switch ZTP to work</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Create Group of New Scopes from Excel</p></td>
<td class="tableblock halign-left valign-top"><p>Allows creation of large number of DHCP scopes using an Excel Spreadsheet as the source for the data</p></td>
</tr>
</tbody>
</table>
</details>

**DNS Module**
<details>
    <summary>Details</summary>

<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th class="tableblock halign-left valign-top">Function</th>
<th class="tableblock halign-left valign-top">Description</th>
<th class="tableblock halign-left valign-top"></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Create Core Connections</p></td>
<td class="tableblock halign-left valign-top"><p>Establishes SSH Connections to the devices in the CoreRouters list variable</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Get Core Logs</p></td>
<td class="tableblock halign-left valign-top"><p>Gathers the last few log entries from each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Get Core Alarms</p></td>
<td class="tableblock halign-left valign-top"><p>Shows the active system and chassis alarms for each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Get Core Down Interfaces</p></td>
<td class="tableblock halign-left valign-top"><p>Shows any down physical interface that has a description for each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Search Cores for MAC or IP</p></td>
<td class="tableblock halign-left valign-top"><p>Searches all routers ARP table for the given MAC or IP</p></td>
<td class="tableblock halign-left valign-top"><p>can be partial</p></td>
</tr>
</tbody>
</table>
</details>

**SSH Module**
<details>
    <summary>Details</summary>

<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th class="tableblock halign-left valign-top">Function</th>
<th class="tableblock halign-left valign-top">Description</th>
<th class="tableblock halign-left valign-top"></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Create Core Connections</p></td>
<td class="tableblock halign-left valign-top"><p>Establishes SSH Connections to the devices in the CoreRouters list variable</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Get Core Logs</p></td>
<td class="tableblock halign-left valign-top"><p>Gathers the last few log entries from each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Get Core Alarms</p></td>
<td class="tableblock halign-left valign-top"><p>Shows the active system and chassis alarms for each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>Get Core Down Interfaces</p></td>
<td class="tableblock halign-left valign-top"><p>Shows any down physical interface that has a description for each router</p></td>
<td class="tableblock halign-left valign-top"></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Search Cores for MAC or IP</p></td>
<td class="tableblock halign-left valign-top"><p>Searches all routers ARP table for the given MAC or IP</p></td>
<td class="tableblock halign-left valign-top"><p>can be partial</p></td>
</tr>
</tbody>
</table>
</details>

**Config Module**
<details>
    <summary>Details</summary>

<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th class="tableblock halign-left valign-top">Function</th>
<th class="tableblock halign-left valign-top">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>Switch Config Template</p></td>
<td class="tableblock halign-left valign-top"><p>Creates switch config template pulled from default network directory and creates DHCP and DNS entries</p></td>
</tr>
</tbody>
</table>
</details>

------------------------------------------------------------------------

## Installation

Clone repository into your Modules folder

Your modules folder can be verified using the following

    # Shows first module path, usually your default
    Write-Output $env:PSModulePath.Substring(0,$env:PSModulePath.IndexOf(';'))

    OR

    # Shows all possible modules paths
    Write-Output $env:PSModulePath

Once the files are in the correct directory (re)start PowerShell.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="icon"><div class="title">
Caution
</div></td>
<td class="content">Changing the policy can allow arbitrary scripts to run which may be dangerous.</td>
</tr>
</tbody>
</table>

If you have errors running the module due to execution policy, use the
following command to allow all scripts to be run without warnings.

    # Run in an Administrative PowerShell Prompt
    Set-ExecutionPolicy Unrestricted

### Configuration

Once all the files are in place you should see the header from NOCTools
when starting PowerShell.

For NOCTools to work properly you will need to properly configure the
variables file located in `$PROFILE/Modules/NOCTools/NOCTools.ps1`

Open the file with your favorite text editor and enter the information
that makes sense for your organization.

**Variables**
<details>
    <summary>Details</summary>

<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th class="tableblock halign-left valign-top">Variable</th>
<th class="tableblock halign-left valign-top">Purpose</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>$Domains.TLD</p></td>
<td class="tableblock halign-left valign-top"><p>Array of Top Level Domains managed by the DNS server</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>$Public Space</p></td>
<td class="tableblock halign-left valign-top"><p>The /16 address of public IP address space for your network</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>$DHCP Controller0</p></td>
<td class="tableblock halign-left valign-top"><p>The FQDN and hostname of your primary DHCP server</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>$DHCP Controller1</p></td>
<td class="tableblock halign-left valign-top"><p>The FQDN and hostname of your secondary DHCP server</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>$DNS Controller0</p></td>
<td class="tableblock halign-left valign-top"><p>The FQDN and IP address of your DNS Server</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>$Reverse Lookup DNS File</p></td>
<td class="tableblock halign-left valign-top"><p>The network location of the Zone file for reverse lookup</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>$Primary Domain DNS File</p></td>
<td class="tableblock halign-left valign-top"><p>The network location of the Zone file for the Primary Domain lookup</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>$Secondary Domain DNS File</p></td>
<td class="tableblock halign-left valign-top"><p>The network location of the Zone file for the Primary Domain lookup</p></td>
</tr>
<tr class="odd">
<td class="tableblock halign-left valign-top"><p>$Core Routers</p></td>
<td class="tableblock halign-left valign-top"><p>List of FQDNs for the primary routers in the network</p></td>
</tr>
<tr class="even">
<td class="tableblock halign-left valign-top"><p>$extreme_440_template</p></td>
<td class="tableblock halign-left valign-top"><p>Path to the template file for an Extreme X440 Network Switch</p></td>
</tr>
</tbody>
</table>
</details>