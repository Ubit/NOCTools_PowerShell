param(
    $NewIPScope,
    [string]$ScopeNetAddr,
    [string]$NetworkName,
    [ValidateSet("y","n")]$EnableDynamicDNS,
    [boolean]$AcceptAll,
    [string]$Gateway,
    [boolean]$Batch
)
If (!$Batch -and !$NewIPScope){
    If (!$ScopeNetAddr) {
        $ScopeNetAddr = Read-Host -Prompt 'Input Scope Network Address I.E. 192.168.0.0/24 or 192.168.0.0/255.255.255.0'
        $NewIPScope = Get-ScopeBreakdown -NetworkScope $ScopeNetAddr
    }
    If ($NewIPScope.ScopeIsPresent){
        Write-Host -ForegroundColor $Colors.Info "`n--------------------`n$($NewIPScope.ScopeID) already exists!"
        If (!$AcceptAll){
            Get-ScopeInfo -Scope $NewIPScope
            Write-Host -ForegroundColor $Colors.Warn  "`nScope creation cancelled`n"
        Break
        }
    }
    If (!$NetworkName) {
        $NewIPScope.ScopeName = Read-Host -Prompt 'Input Scope name I.E. ID-v100 (<COMPLETE BUILDING NAME>)'
    }
    If (!$EnableDynamicDNS -and !$NewIPScope.DynamicDNSUpdates) {
        $EnableDynamicDNS = Read-Host -Prompt "Does Dynamic DNS need to be enabled? (y/n)"
        If ($EnableDynamicDNS -like "*Y*"){$NewIPScope.DynamicDNSUpdates = $True}
        Else {$NewIPScope.DynamicDNSUpdates = $False}
    }
    If (!$Gateway) { $NewIPScope.Gateway = $NewIPScope.EndRange}
    If ($Gateway) { $NewIPScope.Gateway = $Gateway}
}
If (!$AcceptAll -and !$NewIPScope.ScopeIsPresent) {
    Write-Host "`nScope Name: $($NewIPScope.ScopeName) `nScopeID: $($NewIPScope.ScopeID) `nMask: $($NewIPScope.ScopeNetMask) `nScope Range: $($NewIPScope.StartRange) - $($NewIPScope.EndRange) `nRouter Reservation: $($NewIPScope.Gateway) `nDynamic DNS enabled: $($NewIPScope.DynamicDNSUpdates)"
    $Check = Read-Host -Prompt "`nPlease confirm the Scope information above and select (y) to continue"
    If ($Check -like "*Y*"){$AcceptAll = $True}
}
If ($Batch -or $AcceptAll -and !$NewIPScope.ScopeIsPresent){
    Add-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) -Name $NewIPScope.ScopeName -StartRange $NewIPScope.StartRange -EndRange $NewIPScope.EndRange -SubnetMask $NewIPScope.ScopeNetMask -LeaseDuration (New-TimeSpan -Days 30) -State Active -Verbose -ErrorAction Stop
    Add-DhcpServerv4ExclusionRange -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $NewIPScope.ScopeID -StartRange $NewIPScope.Gateway -EndRange $NewIPScope.Gateway -Verbose -ErrorAction Stop
    Set-DhcpServerv4OptionValue -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$($NewIPScope.ScopeID)" -Router $NewIPScope.Gateway -Verbose -ErrorAction Stop
    Add-DhcpServerv4FailoverScope -ComputerName $($DHCPControllers[0].Hostname) -Name "$($DHCPControllers[0].Hostname)-$($DHCPControllers[1].Hostname)" -ScopeId "$($NewIPScope.ScopeID)" -Verbose -ErrorAction Stop
    if ($NewIPScope.DynamicDNSUpdates -eq $True){
        Set-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$($NewIPScope.ScopeID)" -NameProtection $false -DynamicUpdates OnClientRequest -DeleteDnsRROnLeaseExpiry $true -UpdateDnsRRForOlderClients $true -DisableDnsPtrRRUpdate $false -Verbose
        Set-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$($NewIPScope.ScopeID)" -NameProtection $True  -Verbose
    }
    $NewIPScope = New-ScopeObject -ScopeID $NewIPScope.ScopeID
    If ($NewIPScope.ScopeIsPresent){Write-Host -ForegroundColor $Colors.Good "`n----------`nScope creation complete!`n----------`n";Get-ScopeInfo -Scope $NewIPScope}
    Return
}
Write-Host -ForegroundColor $Colors.Warn "`nScope creation cancelled`n"
Return