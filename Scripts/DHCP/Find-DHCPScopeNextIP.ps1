param($ScopeID)
If (!$ScopeID){$scopeid = Read-Host "`nProvide the scopeid of the scope you want to get the next available IP from.`n"}
If (Confirm-ValidIP -IPAddress $ScopeID){
    $ipNum = Read-Host "`nHow many addresses do you need?`n"
    $nextip = Get-DhcpServerv4FreeIPAddress -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $scopeid -NumAddress $ipNum | Format-List | Out-String
    Write-Host -ForegroundColor $Colors.Info "`nNext available IP(s) in $scopeid :"
    Write-Host "$nextip`n"
}