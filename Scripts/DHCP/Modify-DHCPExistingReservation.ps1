param ($IP)
If (!$IP){$IP = New-IPObject -IPAddress $(Read-Host -Prompt "IP to reserve")}
If ($IP.LeaseIsPresent -and $IP.IsReserved) {
    $OldIP = New-IPObject -IPAddress $IP.IP
    Write-Host -ForegroundColor $Colors.Info "`n------------`n$($IP.IP) is already reserved!`n-------------"
    Get-IPInfo -IPInfo $IP
    Write-Host -NoNewLine -ForegroundColor $Colors.Info "`nWould you like to make changes to the existing reservation?(y/n)"
    $Check = Read-Host
    If ($Check -ieq "Y"){
        Write-Host -ForegroundColor $Colors.Info "`nPlease indicate how you would like the record updated. Press enter to leave unchanged."
        $ReservedMAC = Read-Host -Prompt "`nEnter modified MAC address ($($IP.MAC))"
        If ($ReservedMAC){$IP.MAC = Format-MACAddress -InputMAC $ReservedMAC -Format "-"}
        [string]$Hostname = $(Read-Host -Prompt "`nEnter modified Hostname ($($IP.Hostname))")
        If ($Hostname){$IP.Hostname = $Hostname}
        [string]$Description = $(Read-Host -Prompt "`nEnter modified Description ($($IP.Description))")
        If ($Description){$IP.Description = $Description}
    }
    If ($IP.MAC -eq $OldIP.MAC -and $IP.Hostname -eq $OldIP.Hostname -and $IP.Description -eq $OldIP.Description) {
        Write-Host -ForegroundColor $Colors.Warn "`nNo changes requested. Record Modification cancelled`n"
        Break
    }
    Write-Host -ForegroundColor $Colors.Info "`n------------Existing reservation information------------"
    Get-IPInfo -IPInfo $OldIP
    Write-Host -ForegroundColor $Colors.Info "--------------------------------------------------------`n"
    Write-Host -ForegroundColor $Colors.Info "`n------------Modified reservation information------------"
    Get-IPInfo -IPInfo $IP
    Write-Host -ForegroundColor $Colors.Info "--------------------------------------------------------`n"
    Write-Host -NoNewLine -ForegroundColor $Colors.Info "`nWould you like to continue and modify the record?(Y/N)"
    Try {Set-DhcpServerv4Reservation -ComputerName $DHCPControllers[0].Hostname -IPAddress $IP.IP -Description $IP.Description -ClientId $IP.MAC -Name $IP.Hostname -Confirm -ErrorAction Stop}
    Catch {
        Write-Host -ForegroundColor $Colors.Warn "`nUnable to make requested changes to record. Please verify the information and try again.`n"
        Break
    }
    $Test = New-IPObject -IPaddress $IP.IP
    If ($IP.MAC -eq $Test.MAC -and $IP.Hostname -eq $Test.Hostname -and $IP.Description -eq $Test.Description) {
        Write-Host -ForegroundColor $Colors.Info "`nReservation updated successfully`n"
        Get-IPInfo -IPInfo $IP
        Return $True
    }
}
Return $False