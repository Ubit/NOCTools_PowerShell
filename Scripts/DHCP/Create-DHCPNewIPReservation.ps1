param ($IP,[string]$MAC,[string]$Description,[string]$Hostname,$ScopeID)
If ($IP -and !$IP.IP){$IP = New-IPObject -IPaddress $IP}
If (!$IP){$IP = New-IPObject -IPAddress $(Read-Host -Prompt "IP to reserve")}
If (!$IP.LeaseIsPresent){
    Write-Host -ForegroundColor $Colors.Info "`n---------`nIP does not exist. Creating new reservation for $($IP.IP)`n---------`n"
    If (!$ScopeID){$ScopeID = Read-Host -Prompt "What is the scope for the IP?"}
    $Scope = New-ScopeObject -Scope $ScopeID
    If ($Scope.ScopeIsPresent){
        If (!$(Confirm-IPinScopeRange -IP $IP -Scope $Scope)){
            Write-Host -ForegroundColor $Colors.Warn "`nRequested reservation for $($IP.IP) is not available in the scope $($Scope.ScopeID).`n"
            Break
        }
        Write-Host -ForegroundColor $Colors.Good "`nScope $($Scope.ScopeID) found:"
        Get-ScopeInfo -Scope $Scope
        If ($MAC){$IP.MAC = $MAC}
        If ($Description){$IP.Description = $Description}
        If ($Hostname){$IP.Hostname = $Hostname}
        While (!$IP.MAC){$IP.MAC = Read-Host -Prompt "`nMAC to assign reservation"}
        $IP.MAC = Format-MACAddress -InputMAC $IP.MAC -Format "-" 
        While (!$IP.Description) {$IP.Description = Read-Host -Prompt "`nReservation description (FP 123456)"}
        While (!$IP.Hostname){$IP.Hostname = Read-Host -Prompt "`nHostname for the reservation"}
        Add-DhcpServerv4Reservation -ComputerName $DHCPControllers[0].Hostname -ScopeId $Scope.ScopeID -IPAddress $IP.IP -ClientId "$($IP.MAC)" -Description "$($IP.Description)" -ReservationName "$($IP.Hostname)" -Confirm -ErrorAction Stop
        $IP = New-IPObject -IPaddress $IP.IP
        If ($IP.IsReserved){
            Get-IPInfo -IP $IP.IP
            Write-Host -ForegroundColor $Colors.Good "`nReservation of $($IP.IP) complete!`n"
            Return $True
        }
        Write-Host -ForegroundColor $Colors.Warn "IP Reservation failed"
    }
    If (!$Scope.ScopeIsPresent){
        Throw "Scope is not present, cannot reserve IP"
    }
}
Return $False