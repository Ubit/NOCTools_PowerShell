<# This tool requires the use of an externally installed module you can get it by the following
Install-Module ImportExcel -scope CurrentUser
Github for code found here https://github.com/dfinke/ImportExcel
#>
param(
    [ValidateSet ("Header","Column")]$Mode,
    [string]$ExcelDocument,
    [string]$ExcelWorksheet,
    [string]$ExcelStartRange,
    [string]$ExcelEndRange,
    [string]$DynamicDNSEnable,
    [string]$AcceptAll
)
If (!$Mode) {
    Write-Host -NoNewLine "The script can pull the information based off of header name or column position which method do you want to use (Default is Column)?(Header,Column)"
    $Mode = Read-Host
} 
If (!$ExcelDocument){
    Write-Host "`nData must be in the following order/format for import from the Excel document.`nCore Site Name || VLAN Name || VLAN Description || Gateway 1st Octet || Gateway 2nd Octet || Gateway 3rd Octet || Gateway 4th Octet || Subnet 1st Octet || Subnet 2nd Octet || Subnet 3rd Octet || Subnet 4th Octet || Network Address 1st Octet || Network Address 2nd Octet || Network Address 3rd Octet || Network Address 4th Octet"
    Write-Host -ForegroundColor $Colors.Info "`nOpening dialog...`nPlease provide the Excel Document for input"
    $ExcelDocument = Get-FileName 
}
If (!$ExcelWorksheet){$ExcelWorksheet = Read-Host -Prompt "`nWhat is the exact name of the worksheet tab you are pulling data from?"}
If (!$Mode){$Mode = "Column"}
If (!$ExcelStartRange){
    If ($Mode -ieq "Column") {$ExcelStartRange = Read-Host -Prompt "`nWhat is the first line with data for import (Usually 2)? This CANNOT be the header line"}
    If ($Mode -ieq "Header") {$ExcelStartRange = Read-Host -Prompt "`nWhat is the first line with data for import (Usually 1)? This MUST be the header line"}
    If (!$ExcelStartRange -and $Mode -ieq "Column") { $ExcelStartRange = "2"}
    If (!$ExcelStartRange -and $Mode -ieq "Header") { $ExcelStartRange = "1"}
}
If (!$ExcelEndRange){$ExcelEndRange = Read-Host -Prompt "`nWhat is the last line for import?"}
If (!$DynamicDNSEnable){$DynamicDNSEnable = Read-Host -Prompt "`nWill all of the scopes that will be created need dynamic DNS enabled? (y/n)"}
if (!$DynamicDNSEnable){$DynamicDNSEnable = "y"}
If (!$ExcelStartRange -and $Mode -ieq "Column") { $ExcelStartRange = "2"}
If (!$ExcelStartRange -and $Mode -ieq "Header") { $ExcelStartRange = "1"}

If (!$ExcelDocument -or !$ExcelWorksheet -or !$ExcelStartRange -or !$ExcelEndRange) {
    Write-Host -ForegroundColor $Colors.Warn "Required Excel document information not provided. Please enter the required information" 
    Write-Host -ForegroundColor $Colors.Good "$ExcelDocument, $ExcelWorkSheet"
    Write-Output $xls
    Break
}
If ($ExcelDocument -and $ExcelWorksheet -and $ExcelStartRange -and $ExcelEndRange) {
    Write-Host -ForegroundColor $Colors.Good "`n----------`nImporting data from spread sheet...`n----------"

    Write-Host -ForegroundColor $Colors.Info "`nMode is $Mode`n"
    If ($Mode -ieq "Header") {
        $xls = Import-Excel -Path $ExcelDocument -WorksheetName $ExcelWorkSheet -StartRow $ExcelStartRange -EndRow $ExcelEndRange
        <# Add alias for VLAN name #>
        $xls | Add-Member -MemberType AliasProperty -Name VLANName -Value code
        <# Add alias for VLAN description #>
        $xls | Add-Member -MemberType AliasProperty -Name Description -Value descript
        <# Add alias for gateway #>
        $xls | Add-Member -MemberType AliasProperty -Name GW1 -Value defrtr1
        $xls | Add-Member -MemberType AliasProperty -Name GW2 -Value defrtr2
        $xls | Add-Member -MemberType AliasProperty -Name GW3 -Value defrtr3
        $xls | Add-Member -MemberType AliasProperty -Name GW4 -Value defrtr4
        <# Add alias for Subnet Mask #>
        $xls | Add-Member -MemberType AliasProperty -Name SN1 -Value netmsk1
        $xls | Add-Member -MemberType AliasProperty -Name SN2 -Value netmsk2
        $xls | Add-Member -MemberType AliasProperty -Name SN3 -Value netmsk3
        $xls | Add-Member -MemberType AliasProperty -Name SN4 -Value netmsk4           
        <# Add alias for Network Scope ID #>
        $xls | Add-Member -MemberType AliasProperty -Name NWS1 -Value nw1
        $xls | Add-Member -MemberType AliasProperty -Name NWS2 -Value nw2
        $xls | Add-Member -MemberType AliasProperty -Name NWS3 -Value nw3
        $xls | Add-Member -MemberType AliasProperty -Name NWS4 -Value nw4           

    }
    If ($Mode -ieq "Column"){
        $:xls=Import-Excel -Path $ExcelDocument -WorksheetName $ExcelWorksheet -HeaderName 'CoreSite','Router','BuildingCode','VLANName','RouterInterface','Description','GW1','GW2','GW3','GW4','SN1','SN2','SN3','SN4','NWS1','NWS2','NWS3','NWS4' -StartRow $ExcelStartRange -EndRow $ExcelEndRange
    }
}
Write-Host -ForegroundColor $Colors.Info "Please review the sample of imported data to ensure it looks correct.."
Write-Output $xls[0]
If (!$AcceptAll){$AcceptAll = Read-Host -Prompt "`nSelect (Y) to execute all changes, Select (V) to verify each change before it is made Or (N) to cancel?(Y/V/N)"}
If ($AcceptAll -like "*Y*"){$AcceptAll = $True}
If ($AcceptAll -like "*N*") {Write-Host "`nCancelling Changes`n"; Break}
If ($AcceptAll -like "*V*"){$AcceptAll = $False}
ForEach ($XLSScope in $xls){
    If ($XLSScope.VLANName) {
        $scope = "$($XLSScope.NWS1).$($XLSScope.NWS2).$($XLSScope.NWS3).$($XLSScope.NWS4)"
        $netmask = "$($XLSScope.SN1).$($XLSScope.SN2).$($XLSScope.SN3).$($XLSScope.SN4)"
        $gateway = "$($XLSScope.GW1).$($XLSScope.GW2).$($XLSScope.GW3).$($XLSScope.GW4)"
        $scopename = "$($XLSScope.VLANName) ($($XLSScope.Description))"
        $NewScope = Get-ScopeBreakdown -NetworkScope "$scope/$netmask"
        If (!$NewScope.ScopeIsPresent){
            $NewScope.Gateway = $gateway
            $NewScope.ScopeNetMask = $netmask
            $NewScope.ScopeName = $scopename
            If ($DynamicDNSEnable -like "*y*"){$NewScope.DynamicDNSUpdates = $True}
            If ($DynamicDNSEnable -notlike "*Y*") {$NewScope.DynamicDNSUpdates = $False}
            New-DHCPScope -NewIPScope $NewScope -Batch $True -AcceptAll $AcceptAll
        }
    }
    If ($NewScope.ScopeIsPresent){   
        Add-FullWidthLine -Color $Colors.Warn
        Write-Host -ForegroundColor $Colors.Warn "`nScope $($NewScope.ScopeID) already exists`n"
        Get-ScopeInfo -Scope $NewScope
        Add-FullWidthLine -Color $Colors.Warn
    }
}