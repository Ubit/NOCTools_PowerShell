param ($IP, $Description)
If (!$IP){$IP = New-IPObject -IPAddress $(Read-Host -Prompt "IP to reserve")}
If (!$IP.IsReserved -and $IP.LeaseIsPresent){
    Write-Host -ForegroundColor $Colors.Info "`n------------`nLease for $($IP.IP) exists. Reserving existing lease...`n-------------`n"
    Get-IPInfo -IPInfo $IP
    If ($Description) {$IP.Description = $Description}
    If (!$Description){
     $IP.Description = Read-Host -Prompt "`nReservation description. Usually ticket number (FP 123456).`n"
    }
    Write-Host -ForegroundColor $Colors.Info "`nPlease review the information below:"
    Write-Host -ForegroundColor $Colors.Info "`n------------------------"
    Get-IPInfo -IPInfo $IP
    Write-Host -ForegroundColor $Colors.Info "`n------------------------`n"
    Get-DhcpServerv4Lease -ComputerName "$($DHCPControllers[0].Hostname)" -IPAddress $($IP.IP) | Add-DhcpServerv4Reservation -ComputerName "$($DHCPControllers[0].Hostname)" -Description "$($IP.Description)" -Confirm
    $IP = New-IPObject -IPAddress $($IP.IP)
    If (!$IP.IsReserved){
        Write-Host -ForegroundColor $Colors.Warn "There was an error, reservation incomplete."
        Get-IPInfo -IPInfo $IP
    }
    If ($IP.IsReserved){
        Write-Host -ForegroundColor $Colors.Good "`nReservation for $($IP.IP) was successfull.`n"
        Get-IPInfo -IPInfo $IP
        Return $True
    }
}
Return $False