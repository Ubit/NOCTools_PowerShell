param ($IP,[string]$scopeid,[string]$description,[string]$hostname,[string]$reservemac)
While (!$IP.IP){
    Write-Host "`nPlease answer the following questions to complete the reservation of an IP`n"
    $IP = New-IPObject -IPAddress $(Read-Host -Prompt 'Requested IP to be reserved (192.168.0.1)')
}
If (New-DHCPNewIPReservation -IP $IP) { Break }
If (Set-DHCPReserveExistingLease -IP $IP) { Break }
If (Edit-DHCPExistingIPReservation -IP $IP) { Break }
Write-Host -ForegroundColor $Colors.Warn "Reservation was not completed."
Return