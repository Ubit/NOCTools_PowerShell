param (
    $scopes
)
$1 = 130..140
$2 = 0..255
foreach ($sub in $1) {
    foreach ($net in $2) {
        [array]$scopes += "10.$sub.$net.0"
    }
}
$lowestscope = "10.130.0.0"
$highestscope = "10.140.255.255"
foreach ($scope in $scopes){
    $scopedetail = New-ScopeObject -ScopeID $scope
    Write-Host "Current scope: $scope"
    if ($scope -ge $lowestscope -and $scope -le $highestscope -and ($scopedetail.ScopeIsPresent)){
        Write-Host "Making changes to $scope"
        Set-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$scope" -NameProtection $False  -Verbose
        Set-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$scope" -DynamicUpdates OnClientRequest -DeleteDnsRROnLeaseExpiry $true -UpdateDnsRRForOlderClients $true -DisableDnsPtrRRUpdate $false -Verbose
        Set-DhcpServerv4DnsSetting -ComputerName $($DHCPControllers[0].Hostname) -ScopeId "$scope" -NameProtection $True  -Verbose
        [array]$output += $scope
    }
}
$output | Set-Content ./DNSChanges.log