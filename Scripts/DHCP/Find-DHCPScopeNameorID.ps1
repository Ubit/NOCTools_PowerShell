param ($DHCP_Search)
If (!$DHCP_Search){$DHCP_Search = Read-Host "`nEnter DHCP scope ID or name (partial or full) you are searching for"}
Write-Host -ForegroundColor $Colors.Info "`nSearching for $DHCP_Search. This may take a few minutes..." 
Add-FullWidthLine -Color $Colors.Info
Get-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) | Where-Object {($_.ScopeId -like "*$DHCP_Search*" -or $_.Name -like "*$DHCP_Search*")}
Add-FullWidthLine -Color $Colors.Info
Write-Host -ForegroundColor $Colors.Info "`nSearch Complete`n"