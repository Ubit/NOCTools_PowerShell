#param (
    #$IP
#)
#If (!$IP){
    #$IP = $(New-IPObject)
#}
#Write-Host -ForegroundColor $Colors.Good "`nInfo for IP $($IP.IP):"
#Write-Output $IP
param ([string]$IP,$IPInfo)
If (!$IPInfo.IP){
    If (!$IP){$IP = Read-Host -Prompt "`nWhat IP do you want to lookup?"}
    $IPInfo = New-IPObject -IPaddress $IP
}
If ($IPInfo.IPType -eq "IPv6"){Throw "IPv6 is not currently supported on our network."}
If ($IPInfo.LeaseIsPresent){
    Write-Host -ForegroundColor $Colors.Good "`nInformation for IP $($IPInfo.IP):"
    Add-FullWidthLine
    Write-Host "IP: $($IPInfo.IP)`nMAC: $($IPInfo.MAC)`nHostname: $($IPInfo.Hostname)`nLease Status: $($IPInfo.LeaseStatus)`nScope Name: $($IPInfo.ScopeName)`nScope ID: $($IPInfo.ScopeID)`nScope Mask: $($IPInfo.ScopeCIDR)"
    If ($IPInfo.SuperScopeName){Write-Host -NoNewLine "SuperScope Name: $($IPInfo.SuperScopeName)`nSS Scopes List: $($IPInfo.SSIncludedScopes)`n"}
    If ($($IPInfo.Description)){Write-Host -NoNewLine "IP Description: $($IPInfo.Description)`n"}
    Add-FullWidthLine
    Return
}

If (!$IPInfo.LeaseIsPresent){
    Write-Host -ForegroundColor $Colors.Warn "`n----------`nThere are no active lease for $IP on $($DHCPControllers[0].Hostname)`n----------`n`n"
    Return
}
Return $false
