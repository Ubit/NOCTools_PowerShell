<# Request IP information from the user #>
    $scope = Read-Host "What is the scope of the IPs you want to add the Aruba options to? (192.168.0)"
    Write-Host "Now enter the last octet of the IP(s) you want to modify. Entering the same number twice will modify only a single IP."
    $ipbot = Read-Host "First IP you want to change in the scope (last octet only e.g. 1)"
    $iptop = Read-Host "Last IP you want to change in the scope (last octet only e.g. 10)"
    Write-Host -ForegroundColor $Colors.Good "`n----------Be aware that this requires the IPs to already be reserved in the scope---------`n"
    Write-Host -ForegroundColor $Colors.Good "`n----------Remember to remove the options from the scope, this tool will not do it for you---------`n"
    <# Create an Array that creates the range of IPs to change #>
    [array]$ip = $($ipbot..$iptop)

    <# Combine the variables into complete IP addresses and add them to the array $nodes #>
    $nodes = @( $($ip) | ForEach-Object { "$($scope).$_" } )

    $Option43 = "0x31", "0x30", "0x2E", "0x32", "0x30", "0x31", "0x2E", "0x30", "0x2E", "0x33", "0x34"
    $Option60 = "ArubaAP"

    <# For loop that takes each IP listed in $nodes and runs through the commands below for each IP #>
    ForEach ($node in $nodes) {

        <# Display options on IP before making any changes in case it needs to be changed back #>
        Write-Host -ForegroundColor $Colors.Good "`n----------Beginning of changes for $node---------`n"
        Write-Host -ForegroundColor $Colors.Info "`nExisting Options on $node."
        $ExistingValues = Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -IPAddress $node 
        Write-Output $ExistingValues
        Write-Host -ForegroundColor $Colors.Info "`n------------`n"

        <# Add option 43 to the IP from $nodes #>
        #$O43Diff = Compare-Object -ReferenceObject $Option43 -DifferenceObject $($ExistingValues[0].Value)
        #$O60Diff = $($ExistingValues[1].Value) -ne $Option60
        #If ($O43Diff){
            Write-Host "`nAdding option 43 Aruba Server IP address to $node"
            Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ReservedIP $node -OptionId 43 -Value $Option43 
        #}
        <# Add option 60 to the IP from $nodes #>
        #If ($O60Diff){
            Write-Host "`nAdding option 60 Aruba Specific to $node"
            Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ReservedIP $node -OptionId 60 -Value $Option60 
        #}

        #If (!$O43Diff -and !$O60Diff){
            #Write-Host -ForegroundColor $Colors.Warn "`nOptions already exist, no changes needed or made"
        #}
        <# Display options on IP after making changes to show user what has(n't) changed #>
        #If ($O43Diff -or $O60Diff){
            Write-Host -ForegroundColor $Colors.Info "`nOptions after changes on $node.`n"
            Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -IPAddress $node 
            Write-Host -ForegroundColor $Colors.Info "`n------------`n"
        #}
        Write-Host -ForegroundColor $Colors.Good "`n----------End of changes for $node---------`n"
    }