param ($IPScope,$Scope)
While (!$IPScope.ScopeID){$IPScope = New-ScopeObject -scopeid $(Read-Host -Prompt "From which scope do you want to list the leases?")}
If ($IPScope.ScopeIsPresent){
    Add-FullWidthLine
    Write-Host -ForegroundColor $Colors.Good "IPs for Scope $($IPScope.ScopeID):"
    Write-Output $IPScope.ScopeLeases |Format-Table
    Add-FullWidthLine
}
If (!$IPScope.ScopeIsPresent){
    Write-Host -ForegroundColor $Colors.Warn "`n$($IPScope.ScopeID) Does not exist on $($DHCPControllers[0].Hostname)`n"
    Return
}
Return $false
