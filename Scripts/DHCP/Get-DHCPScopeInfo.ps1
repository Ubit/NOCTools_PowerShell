param ($Scope)
If (!$Scope){$Scope = New-ScopeObject -ScopeID $(Read-Host -Prompt "What is the Scope ID?")}
If (!$Scope.ScopeIsPresent){ Write-Host -ForegroundColor $Colors.Warn "`nScope does not exist`n"; Return}
If ($Scope.ScopeIsPresent){
    Add-FullWidthLine
    Write-Host -ForegroundColor $Colors.Good "General Scope Information:"
    Write-Host "Scope Name: $($Scope.ScopeName)`nScope State: $($Scope.ScopeState)`nScope ID: $($Scope.ScopeID)`nScope Mask: $($Scope.ScopeNetMask)`nScope StartRange: $($Scope.StartRange)`nScope EndRange: $($Scope.EndRange)`nLease Duration: $($Scope.LeaseDuration)"
    Write-Host -ForegroundColor $Colors.Good "`nScope Usage Statistics:"
    Write-Host "Available Addresses: $($Scope.AvailableAddresses)`nAddresses in use: $($Scope.AddressesInUse)`nPercentage in use: $($Scope.PercentageInUse)`nAddresses reserved: $($Scope.AddressesReserved)"
    If ($Scope.SuperScopeName){Write-Host "SuperScope Name: $($Scope.SuperScopeName)`nSS Scopes List: $($Scope.SSIncludedScopes)"}
    Write-Host -NoNewLine -ForegroundColor $Colors.Good "`nDHCP Options (Non-Inherited):" 
    Write-Output $Scope.ScopeOptions | Format-table
    Write-Host -ForegroundColor $Colors.Good "Dynamic DNS Settings:"
    Write-Host "Mode: $($Scope.DynamicDNSUpdates)`nName Protection Enabled: $($Scope.DNSNameProtection)`nAuto Delete Reverse Record: $($Scope.DeleteDNSRROnLeaseExpiry)`nDisable PTR Update: $($Scope.DisableDnsPtrRRUpdate)"
    Add-FullWidthLine
}
