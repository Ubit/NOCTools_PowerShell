Get-SSHCoreRouterConnectionStatus
$Routers = Select-SSHRouter
foreach ($Router in $Routers.Host){
    Write-Host $Router
    $Config = Invoke-SSHJunosCommandXML -Device $Router -Command "show configuration | display xml"
foreach ($Interface in $Config.node.configuration.interfaces.interface){
    Write-Host -ForegroundColor $Colors.Good "$($Interface.name) - $($Interface.description)"
        if ($Interface.Name -ne "lo0"){
            foreach ($Unit in $Interface.unit){
                Write-Host -ForegroundColor $Colors.Info "$($Unit.Name) - $($Unit.description)"
                    foreach ($Gateway in $Unit.family.inet.address.name){
                        Write-Output $Gateway
                        $Scope = Get-ScopeBreakdown -NetworkScope $Gateway
                        If ($Scope.ScopeIsPresent){
                            Write-Host -ForegroundColor $Colors.Good "Present"
                        }
                        If (!$Scope.ScopeIsPresent){
                            Write-Host -ForegroundColor $Colors.Warn "Not Present"
                        }
                    }
            }
        }
    #search for network ID from gateway in dhcp Scopes
}
#if found yay
#if not found boo
}