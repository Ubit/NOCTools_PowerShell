<#
check if ip exists in dhcp
check if existing IP matches reserved IP
if matching reserve existing IP and mac matches list
if not matching delete current lease then reserve new IP with mac
#>
param ($IP,$MAC,$ReserveIP,$CSVFile)
$CSV = Import-CSV -Delimiter ',' -Path $CSVFile
$Description = "Description"
$Opt66Value = "PXE_Boot_Server_IP"
$Opt67Value = "ipxe.efi"
#Write-Output $CSV
foreach ($Reservation in $CSV) {
   $IP = New-IPObject -IPaddress $Reservation.IP 
   $ReserveIP = New-IPObject -IPAddress $Reservation.Reserved_IP
   Write-Host -ForegroundColor $Colors.Good "$($IP.IP)" 
   Write-Host -ForegroundColor $Colors.Info "$($ReserveIP.IP)" 
   If (!$IP.IsReserved){
   If (!$ReserveIP.LeaseIsPresent -or $IP.IP -ceq $ReserveIP.IP){
       If ($IP.LeaseIsPresent -and $IP.IP -ceq $ReserveIP.IP){
           Write-Host "Equal" 
           Set-DHCPReserveExistingLease -IP $IP -Description $Description
        }
       If ($IP.LeaseIsPresent -and $IP.IP -ne $ReserveIP.IP){
           Write-Host "Not Equal"
           Remove-DhcpServerv4Lease -IPaddress $IP.IP -Computername $($DHCPControllers[0].Hostname) -Confirm
           New-DHCPNewIPReservation -IP $ReserveIP.IP $Reservation.Mac -Hostname $Reservation.Name -ScopeID $Reservation.ScopeID -Description $Description
        }
   }
   If ($ReserveIP.LeaseIsPresent -and $IP.IP -ne $ReserveIP.IP){
       Write-Host -ForegroundColor $Colors.Warn "Requested Reservation IP is already in use!`nResolve the conflict and try again."
   }
   }
   If ($IP.IsReserved){Write-Host -ForegroundColor $Colors.Warn "IP is already reserved"; Get-IPInfo -IP $IP.IP}
   If (!$IP.LeaseIsPresent) {Write-Host "Not Present"}
   $ReserveIP = New-IPObject -IPAddress $Reservation.Reserved_IP
   $ExistingOptions = Get-DhcpServerv4OptionValue -ReservedIP $ReserveIP.IP -ComputerName $DHCPControllers[0].Hostname
   If ($ReserveIP.IsReserved){
        Write-Output $ExistingOptions
        Write-Host -ForegroundColor $Colors.Info "Applying option 66"
        Set-DhcpServerv4OptionValue -ComputerName $DHCPControllers[0].Hostname -OptionId 66 -Value "$Opt66Value" -ReservedIP $ReserveIP.IP
        Write-Host -ForegroundColor $Colors.Info "Applying option 67"
        Set-DhcpServerv4OptionValue -ComputerName $DHCPControllers[0].Hostname -OptionId 67 -Value "$Opt67Value" -ReservedIP $ReserveIP.IP
        Get-IPInfo -IP $ReserveIP.IP
        $ExistingOptions = Get-DhcpServerv4OptionValue -ReservedIP $ReserveIP.IP -ComputerName $DHCPControllers[0].Hostname
        Write-Output $ExistingOptions
   }
}