param ($Server,$ipmacorDNS)
If ($Server -ceq "Primary"){$DHCPServer = $DHCPControllers[0].Hostname}
If ($Server -ceq "Secondary"){$DHCPServer = $DHCPControllers[1].Hostname}
$ipmacordns = Read-Host "Please provide a partial or full MAC (no seperator format) or IP address to search $($DHCPServer) DHCP logs for.`n"

Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearching recent logs of $($DHCPServer) for $ipmacordns. This may take a few minutes...`n------------------------------`n"
grep -Fhis --color $ipmacordns "\\$($DHCPServer)\dhcpaudit\DHCPSrvLog-*" 
Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearch completed.`n------------------------------`n"