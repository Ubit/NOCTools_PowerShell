param($Scope)
If (!$Scope){
    $Scope = New-ScopeObject -ScopeID $(Read-Host -Prompt "`nScope ID that you are deleting (192.168.0.0)")
}
If (!$Scope.ScopeIsPresent){
    Write-Host -ForegroundColor $Colors.Info "`nScope does not exist.`n"
    Break
}
If ($Scope.ScopeIsPresent){
    Get-ScopeInfo -Scope $Scope
    Remove-DhcpServerv4FailoverScope -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $Scope.ScopeID -Name "$($DHCPControllers[0].Hostname)-$($DHCPControllers[1].Hostname)" -confirm
    Remove-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) -ScopeId $Scope.ScopeID 
}
$Scope = New-ScopeObject -ScopeID $Scope.ScopeID
If (!$Scope.ScopeIsPresent){Write-Host -ForegroundColor $Colors.Good "`nScope successfully removed`n"}
If ($Scope.ScopeIsPresent){
    Write-Host -ForegroundColor $Colors.Warn "Scope NOT removed"
    Get-ScopeInfo -Scope $Scope
    Return $false
}
Return $false