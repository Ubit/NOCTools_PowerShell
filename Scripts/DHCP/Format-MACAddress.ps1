param (
    $InputMAC,
    [ValidateSet ("No-Separator",":",".","-")]$Format,
    [ValidateSet (4,2,0)]$Grouping,
    [ValidateSet ("Uppercase","Lowercase")]$Case
)
If ($Verbose -eq ""){$Verbose = $true}
If (!$Grouping){$Grouping = 2}
If (!$Format) {$Format = ":"}
$MACAddress = $InputMAC -replace "[^a-fA-F0-9]", ""
If ($Case -ne "Uppercase"){$MACAddress = $MACAddress.ToLower()}
If ($Case -eq "Uppercase"){$MACAddress = $MACAddress.ToUpper()}
If ($MACAddress.Length -ne 12) {
    If ($Verbose -eq $true){
        Write-Host -ForegroundColor $Colors.Warn "`nMAC address format incorrect please check entry and try again.`n"
    }
    Return $False
}
If ($Format -eq "No-Separator"){Return $MACAddress}
If ($Grouping -eq 2) {
    $MACAddress = $MACAddress.Insert(10, $Format).Insert(8, $Format).Insert(6, $Format).Insert(4, $Format).Insert(2, $Format)
    Return $MACAddress
}
If ($Grouping = 4) {
    $MACAddress = $MACAddress.Insert(8, $Format).Insert(4, $Format)
    Return $MACAddress
}
Return $False