If (!$IP){$IP = New-IPObject -IP $(Read-Host -Prompt "`nReserved IP to be removed (192.168.0.1)`n")}
If (!$IP.IsReserved){
    Write-Host -ForegroundColor $Colors.Warn "`n$($IP.IP) is not reserved. Cancelling record deletion.`n"
    Break
}
Get-IPInfo -IP $IP.IP
If ($IP.IsReserved){
    Remove-DhcpServerv4Reservation -ComputerName $DHCPControllers[0].Hostname -IPAddress $($IP.IP) -Confirm -ErrorAction Stop
}
$IP = New-IPObject -IP "$($IP.IP)"
If (!$IP.IsPresent){
    Write-Host -ForegroundColor $Colors.Good "`nIP has been successfully removed.`n"
    Return $True
}
If ($IP.IsPresent){Write-Host -ForegroundColor $Colors.Warn "`nIP has not been removed.`n"}
Return $False