$scope = Read-Host "What is the IP scope you want to add the Dell options to? (192.168.0.0)"
param ($Opt66Value,$Opt67Value)
If (Confirm-IPv4Address -IP $ScopeID -or Confirm-IPv6Address -IP $ScopeID){
    ValidateIPorDomain -IP $scope -Display "2"
    Write-Host -ForegroundColor $Colors.Good "`n----------Beginning of changes for $scope---------`n"
    Write-Host "`nExisting Options on $scope."
    Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $scope 
    Write-Host "`nAdding option 66 Server IP address to $scope"
    Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $scope -OptionId 66 -Value $Opt66Value -Confirm
    Write-Host "`nAdding option 67 Bootfile to $scope"
    Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $scope -OptionId 67 -Value $Opt67Value -Confirm
    Write-Host "`nOptions after changes on $scope."
    Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $scope 
    Write-Host -ForegroundColor $Colors.Good "`n----------End of changes for $scope---------`n"
}