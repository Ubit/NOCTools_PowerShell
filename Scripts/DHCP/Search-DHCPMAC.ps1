param ($MAC)
If (!$Mac){$MAC = Read-Host "`nPlease provide the full MAC address to search $($DHCPControllers[0].Hostname) for.`n"}
$MAC = Format-MacAddress -InputMAC $MAC -Format "-" -Grouping 2
If ($MAC){
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearching $($DHCPControllers[0].Hostname) for $MAC.`n------------------------------`n"
    Get-DhcpServerv4Scope -ComputerName $($DHCPControllers[0].Hostname) | Get-DhcpServerv4Lease -ea silentlycontinue -ComputerName $($DHCPControllers[0].Hostname) -ClientId $MAC
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearch completed.`n------------------------------`n"
}