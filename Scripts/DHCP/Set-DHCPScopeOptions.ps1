#10.137.0.0/24 through 10.137.244.0/24
#10.138.1.0/24 through 10.138.69.0/24
$DNSServers = DNSServer1,DNSserver2,etc 
#$DNSServers = 8.8.8.8,8.8.4.4
$scope1 = 0 .. 244
$scope2 = 1 .. 69

Foreach ($IP in $scope1) {
    $scopeip +=  @("10.137.$IP.0")
}
Foreach ($IP in $scope2) {
    $scopeip +=  @("10.138.$IP.0")
}

Foreach ($IP in $scopeip){
    Add-FullWidthLine
    Write-Host -ForegroundColor $Colors.Info "Checking that the scope $IP exists..."
    $Scope = New-ScopeObject -ScopeID $IP
    If ($Scope.ScopeIsPresent){
        Write-Output $Scope.ScopeID
        Write-Host -ForegroundColor $Colors.Info "`nGetting current scope options...`n"
        Try {Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $Scope.ScopeID -OptionId 6 -SilentlyContinue}
        Catch{Write-Host "No DNS option set"}
        Write-Host -ForegroundColor $Colors.Info "`nAdding DNS servers to scope..."
        #Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $Scope.ScopeID -OptionId 6 -Value 8.8.8.8,8.8.4.4 -Force
        Set-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $Scope.ScopeID -OptionId 6 -Value $DNSServers -Force
        Write-Host -ForegroundColor $Colors.Info "`nGetting current scope options...`n"
        Get-DhcpServerv4OptionValue -ComputerName "$($DHCPControllers[0].Hostname)" -ScopeId $Scope.ScopeID -OptionId 6
    }
    If (!$Scope.ScopeIsPresent){Write-Host "Unable to locate the requested scope"}
    Add-FullWidthLine
}