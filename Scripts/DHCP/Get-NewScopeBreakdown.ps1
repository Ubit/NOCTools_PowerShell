param (
    [string]$NetworkScope
)
If (!$NetworkScope){
    Read-Host -Prompt "Please enter the scope you wish to process in CIDR or Standard format (192.168.0.0/24 or 192.168.0.0/255.255.255.0)"
}
<# Process entry to obtain network information #>
$nwstats = Get-NetworkSummary $NetworkScope
$Scope = New-ScopeObject -ScopeID $nwstats.NetworkAddress
$Scope.ScopeNetMask = $nwstats.Mask
<# Get list of all IPs in network range #>
If (!$Scope.ScopeIsPresent){
    $Scope.ScopeLeases = Get-NetworkRange "$($Scope.ScopeID)/$($Scope.ScopeNetMask)"
    $Scope.StartRange = $Scope.ScopeLeases | Select-Object -first 1
    $Scope.EndRange = $Scope.ScopeLeases | Select-Object -last 1
}
If ($Scope){Return $Scope}
Return $false