param ($DNSRecord,$Destination)
If (!$Destination){$Destination = Read-Host -Prompt "What is the modified destination for the record?"}
If (!$DNSRecord.IsPresent) {
    Write-Host -ForegroundColor $Colors.Warn "`n$($DNSRecord.RecordType) record modification cannot continue, no $($DNSRecord.RecordType) record for $RecordName exists.`n"
    Break
}
$DNSRecordPS = Get-DNSRecordData -Domain $DNSRecord
$ModifiedRecord = New-DomainObject -FQDN $DNSRecord.FQDN
$ModifiedRecordPS = Get-DNSRecordData -Domain $ModifiedRecord
If ($($DNSRecord.RecordType) -ieq "CNAME" -and $(Confirm-WebDomain -Domain $Destination)) {
    $ModifiedRecord.Destination = $Destination
    $ModifiedRecordPS.RecordData.HostNameAlias = $Destination
}
elseif ($($DNSRecord.RecordType) -ieq "A" -and $(Confirm-IPv4Address -IP $Destination)) {
    $ModifiedRecord.Destination = $Destination
    $ModifiedRecordPS.RecordData.IPv4Address = [System.Net.IPaddress]::parse($Destination)
}
elseIf ($($DNSRecord.RecordType) -ieq "AAAA" -and $(Confirm-IPv6Address -IP $Destination)) {
    $ModifiedRecord.Destination = $Destination
    $ModifiedRecordPS.RecordData.IPv6Address = [System.Net.IPaddress]::parse($Destination)
}
Else {
    Write-Host -ForegroundColor $Colors.Info "Requested destination does not match the current record type."
    Break
}
<# List existing record details #>
Write-Host -ForegroundColor $Colors.Info "`n------------Existing Record Details------------`n"
Write-Output $DNSRecord
Write-Host -ForegroundColor $Colors.Info "`n-----------------------------------------------`n"
<# List new record details #>
Write-Host -ForegroundColor $Colors.Info "`n------------New Record Details------------`n"
Write-Output $ModifiedRecord
Write-Host -ForegroundColor $Colors.Info "`n------------------------------------------`n"
     
Write-Host -ForegroundColor $Colors.Info "`nDoes the information above look correct?(y/n)"
$Check = Read-Host
If ($Check -ieq "y"){ 
    <# Modify the record with the new information #>
    Set-DnsServerResourceRecord -NewInputObject $ModifiedRecordPS -OldInputObject $DNSRecordPS -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($DNSRecord.TopLevelDomain)"
    <# Compare the record to the requested changes. If either variable is Null it will error out, good comparison gives no output #>
    $DNSRecord = New-DomainObject -FQDN $DNSRecord.FQDN
    $DNSRecordPS = Get-DNSRecordData -Domain $DNSRecord
    Try{
        $Compare = Compare-Object -ReferenceObject $DNSRecordPS -DifferenceObject $ModifiedRecordPS -ErrorAction SilentlyContinue
    }
    Catch{
        Write-Host -ForegroundColor $Colors.Warn "`nThe record on the server does not match the requested changes, there was an error processing your record change."
    }
    <# Verify that the new record is present and that it is equal to the requested changes #>
    If ($DNSRecord.IsPresent -and !$Compare){
        Write-Host -ForegroundColor Green "`nRecord has been successfully modified.`n"
        $DNSRecord
    }
    <# If changes do not equal the record error out #>
    If ($Compare -or !$DNSRecord.IsPresent) {
        Write-Host -ForegroundColor $Colors.Warn "`n$($DNSRecord.RecordType) record modification did not succeed.`n"
        Break
    }
    Break
}
Else { Write-Host -ForegroundColor $Colors.Warn "`n$($DNSRecord.RecordType) record modification cancelled.`n" ; Break}
 
Return