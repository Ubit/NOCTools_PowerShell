param ($DNSRecord)
If (!$DNSrecord){
    Write-Host "`nPlease enter the FQDN that you want to remove from DNS:"
    $DNSRecord = New-DomainObject -FQDN "$(Read-Host)"
}
If (!$DNSRecord.IsPresent) { 
    Write-Host -ForegroundColor $Colors.Warn "`nExisting record not found for $RecordName"
    Write-Host -ForegroundColor $Colors.Info "`nDo you wish to make create a new record?(y/n)"
    $Changes = Read-Host
    If ($Changes -ieq "y"){
        Set-DNSRecord -RecordType "$RecordType" -Change "Add" -RecordName $RecordName -Destination $Destination
        Break
    }
    Write-Host -ForegroundColor $Colors.Warn "`nRecord deletion cannot continue, no record to delete.`n"
    Break
}

If ($DNSRecord.IsPresent){
    Get-DNSRecordData -Domain $DNSRecord
    Write-Host -ForegroundColor $Colors.Info "`nAre you sure you wish to delete this record?"
    Remove-DnsServerResourceRecord -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($DNSRecord.TopLevelDomain)" -Name "$($DNSRecord.Subdomain)" -RRType $($DNSRecord.RecordType)
    $DNSRecordPost = New-DomainObject -FQDN $($DNSRecord.FQDN)
}

If (!$DNSRecordPost.IsPresent){Write-Host -ForegroundColor $Colors.Good "`nRecord successfully deleted.`n"}

If ($DNSRecordPost.IsPresent -and $DNSrecord -Contains $DNSRecordPost){
    Write-Host -ForegroundColor $Colors.Warn "`nRecord deletion failed."
    Break
}

If ($DNSRecord.IsPresent -and $DNSrecord -notcontains $DNSRecordPost){
    Write-Host -ForegroundColor $Colors.Info "Record deleted successfully, though additional records with the same name have been found.`nYou may need to run the command multiple times to delete all the records."
    Get-DNSRecordData -Domain $DNSRecordPost
}

Return