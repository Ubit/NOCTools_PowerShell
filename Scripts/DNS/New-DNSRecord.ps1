param ($DNSRecord,[string]$Destination)
If ($DNSRecord -and !$DNSRecord.FQDN){$DNSRecord = New-DomainObject -FQDN $DNSRecord}
If (!$DNSRecord){$DNSRecord = New-DomainObject -FQDN ($(Read-Host -Prompt "Record FQDN to be created or modified"))}
If ($Destination){$DNSRecord.Destination = $Destination}
If (!$DNSRecord.Destination) {$DNSRecord.Destination = Read-Host "`nWhat is the destination address for the new record?`n"}
If (Confirm-IPv4Address -IP $DNSRecord.Destination){$DNSRecord.RecordType = "A"; $Destination = Confirm-IPv4Address -IP $DNSRecord.Destination}
If (Confirm-IPv6Address -IP $DNSRecord.Destination){$DNSRecord.RecordType ="AAAA"; $Destination = Confirm-IPv6Address -IP $DNSRecord.Destination}
If (Confirm-WebDomain -Domain $DNSRecord.Destination){$DNSRecord.RecordType ="CNAME"; $Destination = Confirm-WebDomain -IP $DNSRecord.Destination}
If ($Destination.IP) {$DNSRecord.Destination = $Destination.IP}
If ($Destination.FQDN) {$DNSRecord.Destination = $Destination.FQDN}
Write-Output $DNSRecord
If ($DNSRecord.IsPresent) { 
    Write-Host -ForegroundColor $Colors.Warn "`nExisting record found for $($DNSRecord.FQDN)"
    Write-Host -ForegroundColor $Colors.Info "`nDo you wish to make changes to the existing record?(y/n)"
    $Changes = Read-Host
    If ($Changes -ieq "y"){
        Set-DNSRecord -DNSRecord $DNSRecord
        Break
    }
    Else {
        Write-Host -ForegroundColor $Colors.Warn "Record creation cannot continue due to an existing record."
        Break
    }
}
If (!$DNSRecord.IsPresent) {Write-Host -ForegroundColor $Colors.Good "`nNo conflicting records found for $($DNSRecord.FQDN)."}
If ($DNSRecord.RecordType -ieq "A" -or $RecordType -ieq "AAAA") {$Destination = New-IPObject -IP $DNSRecord.Destination}
If ($DNSRecord.RecordType -ieq "CNAME") {$Destination = New-DomainObject -FQDN $DNSRecord.Destination}
If (!$Destination.IsPresent){Write-Host -ForegroundColor $Colors.Info "The destination server was not found..."}
If ($Destination.IsPresent) {Write-Host -ForegroundColor $Colors.Good "The destination server was found..."}
Write-Host "`nPlease confirm the following: `n$($DNSRecord.RecordType) record hostname: $($DNSRecord.FQDN) `nPointing to: $($DNSRecord.Destination)"
Write-Host -ForegroundColor $Colors.Info "`nAre you sure you want to create the new $($DNSRecord.RecordType) record?"
If ($DNSRecord.SystemTLD){
    If ($DNSRecord.RecordType -ieq"A"){Add-DnsServerResourceRecord -A -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($DNSRecord.TopLevelDomain)" -Name "$($DNSRecord.Subdomain)" -IPv4Address "$($DNSRecord.Destination)" -Confirm}
    If ($DNSRecord.RecordType -ieq "AAAA") {Add-DnsServerResourceRecord -AAAA -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($DNSRecord.TopLevelDomain)" -Name "$($DNSRecord.Subdomain)" -IPv6Address "$($DNSRecord.Destination)" -Confirm}
    If ($DNSRecord.RecordType -ieq "CNAME") {Add-DnsServerResourceRecord -CName -ComputerName $($DNSControllers[0].Hostname) -ZoneName "$($DNSRecord.TopLevelDomain)" -Name "$($DNSRecord.Subdomain)" -HostNameAlias "$($DNSRecord.Destination)" -Confirm}
    $DNSRecord = New-DomainObject -FQDN "$($DNSRecord.FQDN)"
    If ($DNSRecord.IsPresent){Write-Host -ForegroundColor $Colors.Good "`nRecord successfully created.`n"}
    If (!$DNSRecord.IsPresent){
        Write-Host -ForegroundColor $Colors.Warn "`nRecord creation cancelled."
        Break
    }
}
Return