<#
    .SYNOPSIS

    Verifies that required Modules are installed and recognized by PowerShell


    .DESCRIPTION

    Runs through each of the required modules and verifies each is installed.

    If any external modules are missing it will provide you with the option 
    to install it to your local modules directory.

    If any sub-modules are not loading it will give offer to view the errors.

    .INPUTS

    None.

    .OUTPUTS

    None.

    .EXAMPLE

    Confirm-NOCToolsModules
    Checking that required Modules are installed...

        Indented.Net.IP is installed


        ImportExcel is installed


        Posh-SSH is installed


        DhcpServer is installed


        DnsServer is installed

    Verifing NOCTools are properly installed...

        Primary NOCTools module is installed properly.


        NOCTools-DHCP sub-module is working properly.


        NOCTools-DNS sub-module is working properly.


        NOCTools-SSH sub-module is working properly.


        NOCTools-Other sub-module is working properly.

    .EXAMPLE

    Confirm-NOCToolsModules
    Checking that required Modules are installed...

        Indented.Net.IP is installed


        ImportExcel is installed


        Required Module (Posh-SSH) does not exist. Please install to continue.

            Would you like to install the Posh-SSH module to your local modules directory?(y/n)n

        DhcpServer is installed


        DnsServer is installed

    Verifing NOCTools are properly installed...

        Primary NOCTools module is installed properly.


        NOCTools-DHCP sub-module is working properly.


        NOCTools-DNS sub-module does not appear to be working properly.

            Would you like to view the error for the sub-module?n

        NOCTools-SSH sub-module is working properly.


        NOCTools-Other sub-module is working properly.

    .NOTES

    Additional PowerShell tools may be required:

    You must install the RSAT Windows features to enable you to be able to run these commands. 
    Go to Start > Settings > Find "Apps & Features" > Select "Optional Features" > Click + Add a Feature 
    Install the 
    - RSAT: DNS Server Tools 
    - RSAT: DHCP Server Tools
    - RSAT: IP Address Management (IPAM) Client 
    - RSAT: Remote Access Management Tools
    Others you might want to install 
    - RSAT: Active Directory Domain Services 
    - Lightweight Directory Services Tools
    - RSAT: Group Policy tools

    It may not be necessary to manually install these tools depending on your setup
    If you have any issues executing commands with an error like "cmdlet is not available" you may need to install one or more of these
#>
<# Primary Module Name #>
$noctools = @("NOCTools")
Write-Host "Checking that required Modules are installed..."
Foreach ($module in $ExternalModules.ModuleName){
    If (Get-Module -ListAvailable -Name $module) {Write-Host -ForegroundColor $Colors.Good "`n   $module is installed`n"} 
    else {
        Write-Host -ForegroundColor $Colors.Warn "`n   Required Module ($module) does not exist. Please install to continue.`n"
        Write-Host -NoNewLine -ForegroundColor $Colors.Info "     Would you like to install the $module module to your local modules directory?(y/n)"
        $yesno = Read-Host
        If ($yesno -eq "y"){Install-Module -Name $module -Scope CurrentUser}
    }
}
Write-Host "Verifing NOCTools are properly installed..."
Foreach ($module in $NOCTools){
    If (Get-Module -ListAvailable -Name $module) {Write-Host -ForegroundColor $Colors.Good "`n   Primary $module module is installed properly.`n"} 
    Else {Write-Host -ForegroundColor $Colors.Warn "`n Primary $module module is not installed properly.`n"}
}
Foreach ($Module in $SubModules) {
    $checkmodule = Get-Command -ListImported -Name $($Module.ModuleCommand) -errorAction "SilentlyContinue"
    If ($checkmodule) {Write-Host -ForegroundColor $Colors.Good "`n   $($Module.ModuleName) sub-module is working properly.`n"}
    Else {
        Write-Host -ForegroundColor $Colors.Warn "`n   $($Module.ModuleName) sub-module does not appear to be working properly.`n"
        Write-Host -NoNewLine -ForegroundColor $Colors.Info "     Would you like to view the error for the sub-module?"
        $check = Read-Host
        If ($check -ieq "y"){
            Import-Module -Verbose "$($Module.ModuleDirectory)"
        }
    }
}