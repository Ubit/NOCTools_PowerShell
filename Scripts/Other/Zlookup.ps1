param (
    [string]$Domain,
    [string]$Search
)
while (!$domain) {
    $domain = Read-Host -Prompt "`nWhat DNS zone are you searching ($($Domains.TLD[1]), $($Domains.TLD[0]), Reverse)?`n"
}
If ($($Domains.TLD) -notcontains $domain -and $domain -notlike "reverse") {
    Write-Host -ForegroundColor $Colors.Warn "`n------------------------------`nThis command will only operate with the TLDs "$($Domains.TLD[0])", "$($Domains.TLD[1])", or $$(IPSpace[0].Scope).0.0 addresses`n`nUse Syntax: zlookup -Domain [ttu.edu]/[texastech.edu]/[reverse] -Search [pattern]`n------------------------------`n"
    Break
}
while (!$Search) {
    $Search = Read-Host -Prompt "`nWhat partial / full subdomain or IP are you searching for? (example.$domain)`n"
}
If ("$domain" -ieq "reverse") {
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearching $($IPSpace[0].Scope).0.0 for $Search`n------------------------------`n"
    Select-String -Path $DNSControllers[0].ReverseLookupFile -Pattern $Search
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearch Complete`n------------------------------`n"
    Break
}
if ("$domain" -ieq $Domains.TLD[0]) {
    Write-Host -ForegroundColor $colors.Info "`n------------------------------`nSearching $($Domains.TLD[0]) for $Search`n------------------------------`n"
    Select-String -Path $DNSControllers[0].PrimaryDomainFile -Pattern $Search
    Write-Host -ForegroundColor $colors.Info "`n------------------------------`nSearch Complete`n------------------------------`n"
    Break
}
If ("$domain" -ieq $Domains.TLD[1]) {
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearching $($Domains.TLD[1]) for $Search`n------------------------------`n"
    Select-String -Path $DNSControllers[0].SecondaryDomainFile -Pattern $Search
    Write-Host -ForegroundColor $Colors.Info "`n------------------------------`nSearch Complete`n------------------------------`n"
    Break
}