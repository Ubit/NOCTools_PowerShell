param (
    $Device
)
If (!$Device){
    Write-Host "What do you want to connect to"
    $Device = Read-Host
}
$Connection = New-SSHConnection -Server $Device
Start-Sleep 3
Write-Host $Connection
If (!$Connection) {Write-Host -ForegroundColor $Colors.Warn "Connection failed"}
If ($Connection){
    $SwitchOutput = $NULL
    $SwitchOutput = Invoke-SSHCommand -SessionId $Connection.SessionId -command "show vlans | display xml"
    $VLANS = Convert-JuniperXMLtoObject -XMLin $($SwitchOutput[0])
    foreach ($vlan in $VLANS.Node.'vlan-information'.vlan){
        write-host "`n`nVLAN Name: $($vlan."vlan-name")`nVLAN Tag: $($vlan."vlan-tag")`nPorts:"
        foreach ($port in $($vlan."vlan-detail"."vlan-member-list"."vlan-member"."vlan-member-interface")){
            Write-Host -nonewline "$Port, "
        }
    }
}
Remove-SSHSession $Connection