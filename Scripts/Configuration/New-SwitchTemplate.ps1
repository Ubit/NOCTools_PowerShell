param ($Switch,[string]$SwitchName,[ValidateSet("X440","")]$SwitchType,[string]$BuildingName,[string]$UplinkDevice,[string]$SwitchIP,[string]$CIDR,[string]$ManagementVID,$VLANS = @(),$BuildingCode,$Ports)

If (!$SwitchName) {$SwitchName = Read-Host -Prompt "What is the name of the Switch you are configuring?"}
If (!$SwitchType) {$SwitchType = Read-Host -Prompt "`nWhat type of switch are you configuring?(X440)"}
If (!$BuildingName) {$BuildingName = Read-Host -Prompt "`nName of the building $SwitchName will be installed in"}
If (!$UplinkDevice) {$UplinkDevice = Read-Host -Prompt "`nName of uplink device for $SwitchName"}
If (!$SwitchIP) {$SwitchIP = Read-Host -Prompt "`nManagement IP for $SwitchName"}
If (!$CIDR) {$CIDR = Read-Host -Prompt "`nWhat is the CIDR for the IP?"}

$Switch = New-SwitchObject -Switch $SwitchName
if (!$Switchtype) { $Switchtype = "X440" }
if ($SwitchType -ieq "X440"){
    $Switch.VLANConfig = $ExtremeX440.VLANConfig
    $Switch.PortConfig = $ExtremeX440.PortConfig
    $Switch.Template = $ExtremeX440.Template
}
If (!$BuildingCode){
    $BuildingCode = $SwitchName.Substring(0,2)
}
$ManagementVID = Read-Host -Prompt "`nVLAN ID for the management network"
$Check = "1"
$VLANs = @()
While ($Check -notlike "*n*" -or $Check -notlike "") {
    $Description = $NULL
    Write-Host "`nEnter the Name and VLAN ID for each VLAN you are adding to this switch:"
    [int]$ID = Read-Host -Prompt "VLAN ID"
    $NewName = "$BuildingCode-v$ID"
    $Name = Read-Host -Prompt "VLAN Name($NewName)"
    If (!$Name){$Name = $NewName}
    If ($DefaultVLANs.ID -contains $ID){
        $DefaultDescription = $DefaultVLANs | Where-Object -Property "ID" -EQ $ID
        $check = Read-Host -Prompt "Would you like to use the default description for this vlan?($($DefaultDescription.Name))(y/n)" 
        If ($check -like "*y*" -or !$check){
            $Description = $DefaultDescription.Name
        }
    }
    If (!$Description) {$Description = Read-Host -Prompt "VLAN Description"}
    $Ports = Read-Host -Prompt "List the ports you would like this vlan untagged on"

    If ($Name -and $ID -and $Description){
        $VLANS +=[PSCustomObject] @{
            VLANName = "$Name"
            VLANID = "$ID"
            VLANDescription = "$Description"
            Ports = "$Ports"
        }
    }
    Else {Write-Host -ForegroundColor $Colors.Warn "VLAN data missing"}
    Write-Output $VLANS
    $Name = $NULL
    $ID = $NULL
    Write-Host -ForegroundColor $Colors.Info "Would you like to add another VLAN? (yes/no) (default = no)"
    $check = Read-Host
    If ($Check -like "*n*" -or $check -like ""){
        Break
    }
}
ForEach ($VLAN in $VLANS) {
    $VLANConfig += $Switch.VLANConfig 
    If ($VLAN.Ports){ 
        $VLANConfig += $Switch.PortConfig
        $VLANConfig = $VLANConfig -Replace ("<PortsList>","$($VLAN.Ports)")
    }
    $VLANConfig = $VLANConfig -Replace ("<VLANName>","$($VLAN.VLANName)")
    $VLANConfig = $VLANConfig -Replace ("<VLANID>","$($VLAN.VLANID)")
    $VLANConfig = $VLANConfig -Replace ("<VLANDescription>","$($VLAN.VLANDescription)")
}

$Scope = Get-ScopeBreakdown -NetworkScope "$SwitchIP/$CIDR"

If (!$Scope.Gateway){
    $Scope.Gateway = $Scope.EndRange
}



If ($check -ine "y"){

    Write-Host -NoNewLine -ForegroundColor $Colors.Info "`nIs the information above correct?(y/n)"
    $check = Read-Host  

}
If ($check -ieq "y"){

    <# Make changes to config in memory #>
    $Switch.Template = $Switch.Template -Replace ("<SETSYSNAME>","$SwitchName")
    $Switch.Template = $Switch.Template -Replace ("<BUILDINGNAME>","$BuildingName")
    $Switch.Template = $Switch.Template -Replace ("<UPLINKDEVICE>","$UplinkDevice")
    $Switch.Template = $Switch.Template -Replace ("<VLANs>","$VLANConfig")
    $Switch.Template = $Switch.Template -Replace ("<SWITCHIP>","$SwitchIP")
    $Switch.Template = $Switch.Template -Replace ("<CIDR>","$CIDR")
    $Switch.Template = $Switch.Template -Replace ("<GATEWAYIP>","$($Scope.Gateway)")
    $Switch.Template = $Switch.Template -Replace ("<MngmtVLAN>","$ManagementVID")

    <# Write configuration to file#>
    $date = get-date -Format "yyyy-MM-dd.HHmm"
    $Switch.Template | Set-Content -Path "./$($SwitchName)-$($env:Username)_$($date).config"
        
    <# Print file location #>
    $CurrentDirectory = Get-Location
    Write-Host -ForegroundColor $Colors.Good "`nConfiguration has been written to $($CurrentDirectory.Path)\$($SwitchName)-$($env:Username)_$($date).config `n"
}


$dhcpdns = Read-Host -Prompt "Do you wish to setup DNS/DHCP reservations?(y/n)"

if ($dhcpdns -ieq "y"){
    $domain = "$SwitchName.$($Domains.TLD[0])"

    Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nCreating DHCP Reservation `n---------------------"
    If (New-DHCPNewIPReservation -IP $SwitchIP -Hostname $SwitchName -ScopeID $Scope.ScopeID) {
    } 
    Else {
        If (Get-IPData -IP $SwitchIP){
            Write-Host -ForegroundColor $Colors.Info "IP already reserved"
            Get-IPInfo -IP $SwitchIP
        }
    }
    Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nCreating DNS Reservation `n---------------------"
    New-DNSRecord -DNSRecord $Domain -Destination $SwitchIP


    New-RADIUSEntry -Name "$SwitchName" -IP "$SwitchIP" -DeviceType "$SwitchType"

}