param ($CSV)
$csvimport = Import-Csv $CSV
foreach ($vlan in $csvimport) {
    $scope = "$($vlan.defrtr1).$($vlan.defrtr2).$($vlan.defrtr3).$($vlan.defrtr4)"
    $netmask = "$($vlan.netmsk1).$($vlan.netmsk2).$($vlan.netmsk3).$($vlan.netmsk4)"
    $scopedetails = Get-NetworkSummary -IPAddress "$scope/$netmask"
    $vlan.prefix = $scopedetails.CIDRNotation
    $siteprefix = $($vlan.description.split(" ")[-1])
    $vlan.'vlan_group' = "$siteprefix VLANs"
    $output += @($vlan)
}
Return $output
#site,vlan,vlan_group
