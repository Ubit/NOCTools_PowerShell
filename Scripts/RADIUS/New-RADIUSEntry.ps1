param ($Name,$IP,$DeviceType)
Connect-RADIUSServers
Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nChecking for existing RADIUS entry`n---------------------"
$RadiusCheck = & $("Get-NpsRadiusClient") |Where-Object -Property "Name" -Like "*$($Name)*"
If (!$RadiusCheck){
    Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nGathering Template Information`n---------------------"
    $Templates = & $("Get-NpsSharedSecretTemplate")
    If ($DeviceType -ieq "X440"){
        Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nSetting Switch type to Extreme X440`n---------------------"
        $Template = $Templates |Where-Object -Property "Name" -EQ "Extreme"
    }
    Write-Host "Please verify the information below"
    Write-Host "Device Name: $Name`nTemplate Name: $($Template.Name)`nTemplate Secret: $($Template.SharedSecret)"
    $YN = Read-Host -Prompt "`nAre you sure you wish to continue creating the RADIUS entry?"
    If ($Template -and $YN -ieq "Y"){
        Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nCreating RADIUS entry`n---------------------"
        Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nApplying changes to RADIUS Server`n---------------------"
        New-NpsRadiusClient -Name "$Name" -SharedSecret "$($Template.SharedSecret)" -Address "$IP"
        Write-Host -ForegroundColor $Colors.Info "`n--------------------- `nVerifying RADIUS entry`n---------------------"
        $Check = & $("Get-NpsRadiusClient") |Where-Object -Property "Name" -Like "*$($Name)*"
        If ($Check){
            Write-Host -ForegroundColor $Colors.Good "`n--------------------- `nNew Entry Added Successfully`n---------------------"
            Write-Host "`n$($RadiusCheck)"
            Return
        }
        Write-Host -ForegroundColor $Colors.Warn "New RADIUS entry has failed"
        Break
    }
}
Write-Host -ForegroundColor $Colors.Warn "`nRadius Entry Exists"
$RadiusCheck
Break