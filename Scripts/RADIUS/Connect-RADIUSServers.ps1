$CommandCheck = $NULL
<#
$SessionCheck = Get-PSSession
If ($SessionCheck){
    Write-Host -ForegroundColor $Colors.Warn "`nExisting Sessions"
    Write-Output $SessionCheck
    Write-Host -NoNewLine -ForegroundColor $Colors.Info "`nDo you wish to clear the existing sessions?(y/n)"
    $Proceed = Read-Host
    if ($Proceed -notlike "*Y*") {
        Write-Host -ForegroundColor $Colors.Info "`nCancelling RADIUS server(s) connection attempt."
        Return
    }
    Foreach ($Session in $SessionCheck.Name){Remove-PSSession -Name $Session}
    Write-Host -ForegroundColor $Colors.Good "`nAll Sessions have been cleared."
}
#>
<# Import Nps Module functions from each Radius Server #>
<#
Foreach ($server in $RadiusServers){
    Write-Host -ForegroundColor $Colors.Info "`nCreating session for $($server.Name)"
    $Serverconnection = $(New-PSSession -Computername $server.Hostname -Name $server.Name)
    Write-Host -ForegroundColor $Colors.Info "`nImporting NPS Module from $($server.Name)"
    Import-Module -name Nps -PSSession $Serverconnection -Prefix $($server.Name)
    Write-Host -ForegroundColor $Colors.Info "`nVerifying that import was succesfull."
    $CommandCheck = Get-Command -Name Get-$($Server.Name)NpsRadiusClient
    If (!$CommandCheck){
        Write-Host -ForegroundColor $Colors.Warn "`nModule was not imported successfully."
    }
}
#>
$CommandCheck = Get-Command -Name Get-NpsRadiusClient -ErrorAction SilentlyContinue
If (!$CommandCheck){
    Write-Host -ForegroundColor $Colors.Info "`nCreating session for $($RadiusServers[0].Name)..."
    $Serverconnection = $(New-PSSession -Computername $RadiusServers[0].Hostname -Name $RadiusServers[0].Name)
    Write-Host -ForegroundColor $Colors.Info "`nImporting NPS module from $($RadiusServers[0].Name)..."
    Import-Module -name Nps -PSSession $Serverconnection 
    $CommandCheck = Get-Command -Name Get-NpsRadiusClient
    If (!$CommandCheck){
        Write-Host -ForegroundColor $Colors.Warn "`nRadius module was not imported successfully."
        Return $false
    }
    If ($CommandCheck){
        Write-Host -ForegroundColor $Colors.Good "`nRadius module was imported successfully."
        Return 
    }
    Return $false
}
If ($CommandCheck){Write-Host -ForegroundColor $COlors.Info "`nRadius module already loaded."}