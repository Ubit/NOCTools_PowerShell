
param (
    <# DeviceName is the friendly name used for the device profile #>
    [string]$DeviceName,
    <# DeviceIP is the IP for the device #>
    [string]$DeviceIP,
    <# Secret is the actual secret that you wish to use for the profile #>
    [string]$Secret,
    <# Template is the Name of the template that you which to use for the profile #>
    [string]$Template,
    <# The name of the server you will be modifying the profile on #>
    [string]$Server
)

Get-RADIUSServerStatus

<# Which server(s) do you want to change records on #>
While (!$ServerSelection){

    $count = 0
    Write-Host "`n"
    Foreach ($name in $RADIUSServers.Connection.Name) {
        Write-Host "$count. $($RADIUSServers.Connection.Name[$count])"
        $count = $count +1
    }
    Write-Host -NoNewLine "`nOn which server will you be modifying a RADIUS record?"

    $ServerSelection = Read-Host


    If ($ServerSelection -Match "^\d$" -and $ServerSelection -lt $count){
        $Server = $RADIUSServers.Connection[$ServerSelection]

        Write-Output $Server.Name
    }

    ElseIf ($RADIUSServers.Connection.Name -icontains $ServerSelection ){
        $Server = $RADIUSServers.Connection |Where-Object -Property Name -icontains $ServerSelection
        $ServerName = $Server.Name
        Write-Output $Server.Name
    }

    Else {
        Write-Host -ForegroundColor $Colors.Warn "`nInvalid entry please try again."
        $ServerSelection = Out-Null
    }
}
        Write-Output $Server
While (!$Change){
    Write-Host -NoNewLine "`nWhat type of change will you be making (0.Add, 1.Delete, 2.Modify)? "
    $Change = Read-Host

        Write-Output $Server
    If ($Change -ieq "Add" -or $Change -eq "0"){
                    Write-Output $ServerName
        Get-Command -Name "Get-$($ServerName)NpsRadiusClient"
    <# Add #>
    <# Verify inputs are valid #>
    <# Pull data from server #>
    <# Verify there is no existing record with IP or name #>
    <# If record exists offer to delete or modify it #>
    <# Add Record #>
    <# Verify that the record was successfully added and display it #>
    }

    Elseif ($Change -ieq "Delete" -or $Change -eq "1"){
        Get-Command -Name Get-$($Server.Name)NpsRadiusClient
    <# Delete #>
    <# Verify inputs are valid #>
    <# Pull data from the server #>
    <# Verify there is an existing record and display its data #>
    <# Delete Record #>
    <# Verify Record was successfully deleted #>
    }


    ElseIf ($Change -ieq "Modify" -or $Change -eq "2"){
        Get-Command -Name Get-$($Server.Name)NpsRadiusClient
    <# Modify #>
    <# Ask for details to be modified #>
    <# Verify inputs are valid #>
    <# Verify Record exists #>
    <# If no record exists offer to add it #>
    <# Modify Record #>
    <# Display Modified Record#>
    }

    Else {
        Write-Host -ForegroundColor $Colors.Warn "`nInvalid entry please try again."
        $Change = Out-Null
    }
}


