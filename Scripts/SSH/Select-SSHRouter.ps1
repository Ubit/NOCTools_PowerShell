param($Selection)
If ($Selection -ine "All" -and $CoreConnections) {
    $Continue = "N"
    While ($Continue -ine "Y" -and $Continue -ne "" -and $Continue -ine "All") {
        Write-Host "`nIf you would like only a specific router's data please enter the name of that router here"
        $Selection = Read-Host
        $Routers = $global:CoreConnectionNames | Where-Object -Property "Name" -Match "(.*)$Selection(.*)"
        If (!$Routers -or $Continue -ieq "All") {$Routers = $CoreConnections}
        Write-Host "Currently selected Router(s):"
        foreach ($Router in $($Routers.Name)) {
            Write-Host "$Router"
            If (!$Router) {Write-Host -ForegroundColor $Colors.Info "`nNo routers found matching your entry, select (N) to try again or (All) for all routers"}
        }
        If ($Routers){Write-Host "`nDoes this look correct?(Y/N/All)"}
        $Continue = Read-Host
    }
}
$RouterOut = @()
foreach ($Router in $Routers){
$RouterOut += $CoreConnections | Where-Object -Property "Host" -EQ $($Router.IP)
}
Return $RouterOut