Get-SSHCoreRouterConnectionStatus
$Routers = Select-SSHRouter
ForEach ($Router in $($Routers.Host)) {
    $RouterName = Get-RouterName -IP $Router
    $PortDown = Invoke-SSHJunosCommandXML -Device "$Router" -Command "show interfaces descriptions"
    $PortDown = $PortDown.node.'interface-information'.'physical-interface' | Where-Object -Property "admin-status" -EQ "up" | Where-Object -Property "oper-status" -EQ "Down"
    If ($PortDown){
        Write-Host -ForegroundColor $Colors.Info "`nDown port information for $($RouterName.Name) ...`n"
        Write-Host -ForegroundColor $Colors.Info "`n-------------------------`n"
        foreach ($port in $PortDown){Write-Host "Interface: $($Port.Name)`nDescription: $($Port.Description)`nStatus: $($Port.'oper-status')`n"}
        Write-Host -ForegroundColor $Colors.Info "-------------------------`n"
    }
} 