Get-SSHCoreRouterConnectionStatus
$Routers = Select-SSHRouter
ForEach ($Router in $($Routers.SessionID)){
    $RouterName = Get-RouterName -IP $($CoreConnections.Host[$Router])
    Write-Host -ForegroundColor $Colors.Info "`nGathering system information for $($RouterName.Name) ...`n"
    $ChassisAlarms = Invoke-SSHCommand -SessionID $Router -Command "show chassis alarms | display xml"
    $ChassisAlarms = Convert-JuniperXMLtoObject -XMLin $ChassisAlarms
    $ChassisAlarms = $ChassisAlarms.Node.'alarm-information'.'alarm-detail'
    If ($ChassisAlarms){
        foreach ($alarm in $($ChassisAlarms)) {$alarms += "$($alarm.'alarm-class') - $($alarm.'alarm-description') ($($alarm.'alarm-time'.'#text'.Replace('    ','')))`n"} 
        $ChassisAlarms = $alarms
        $alarms = $NULL
    }
    If (!$ChassisAlarms){$ChassisAlarms = "No alarms currently active"}
    $SystemAlarms = Invoke-SSHCommand -SessionID $Router -Command "show system alarms"
    $ChassisEnvironment = Invoke-SSHCommand -SessionID $Router -Command "show chassis environment | display xml"
    $ChassisEnvironment = Convert-JuniperXMLtoObject -XMLin $ChassisEnvironment
    $ChassisEnvironment = $ChassisEnvironment.Node.'environment-information'.'environment-item' | Where-Object -Property "status" -ne "OK"
    If (!$ChassisEnvironment){$ChassisEnvironment = "No alarms currently active"}
    $ChassisRouting = Invoke-SSHCommand -SessionID $Router -Command "show chassis routing-engine"
    $CR = $ChassisRouting.Output | Select-String -Pattern "(.*)Slot(.*)|(.*)Stat(.*)|(.*)Uptime(.*)|(.*)Temp(.*)|(.*)Memory(.*)|(.*)Load(.*)|\."
    If ($ChassisAlarms -or $SystemAlarms -or $ChassisEnvironment -or $ChassisRouting) {
        Write-Host -ForegroundColor $Colors.Good "`n------------Chasis Alarms-------------`n"
        Write-Output $($ChassisAlarms)
        Write-Host -ForegroundColor $Colors.Good "`n------------System Alarms-------------`n"
        Write-Output $($SystemAlarms.Output)
        Write-Host -ForegroundColor $Colors.Good "`n------------Chassis Routing Engine Status-------------`n"
        Write-Output $($CR)
        Write-Host -ForegroundColor $Colors.Good "`n------------Chassis Status-------------`n"
        Write-Output $ChassisEnvironment
        Add-FullWidthLine -Color $Colors.Info
    }
}