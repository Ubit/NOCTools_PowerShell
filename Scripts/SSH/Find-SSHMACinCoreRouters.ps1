param ([string]$Query  )
Get-SSHCoreRouterConnectionStatus
$Routers = Select-SSHRouter
While (!$QueryInput){
    Write-Host "`nPlease enter an IP or MAC to search for."
    $QueryInput = Read-Host
    If ($Query = Format-MACAddress -InputMAC $QueryInput -Verbose $false) {Write-Host "`n$Query is a valid MAC`n"}
    If ($Query = Confirm-IPv4Address -IP $QueryInput) {`nWrite-Host "$Query is a valid IP`n"}
    If ($Query = Confirm-IPv6Address -IP $QueryInput) {`nWrite-Host "$Query is a valid IP`n"}
}
Write-Host -ForegroundColor $Colors.Info "`nChecking Routers ARP for $Query...`n"
ForEach ($Router in $($Routers.SessionID)){
    $Router_Output = Invoke-SSHCommand -SessionId $Router -command "show arp no-resolve | display xml" 
    $Router_Output = Convert-JuniperXMLtoObject $Router_Output
    $Results = $($Router_Output.node."arp-table-information"."arp-table-entry") | Where-Object {($_."ip-address" -eq "$Query") -or ($_."mac-address" -ieq "$Query")}
    If ($Results){
        $SuperInterface = $Results.'interface-name' | Select-String -Pattern "(.*?)\[" | ForEach-Object { $_.Matches.Groups[1].Value }
        If ($SuperInterface){
            $SuperInterface = Invoke-SSHCommand -SessionId $Router -command "show interfaces $($SuperInterface) | display xml"
            $SuperInterface = Convert-JuniperXMLtoObject $SuperInterface
            $SuperInterface = $SuperInterface.Node.'interface-information'.'logical-interface'
            $SubInterface = $Results.'interface-name' | Select-String -Pattern "\[(.*?)\]" | ForEach-Object { $_.Matches.Groups[1].Value }
        }
        If (!$SuperInterface) {$SubInterface = $Results."interface-name"}
        $SubInterface = Invoke-SSHCommand -SessionId $Router -command "show interfaces $($SubInterface) | display xml" 
        $SubInterface = Convert-JuniperXMLtoObject $SubInterface
        $SubInterface = $SubInterface.Node.'interface-information'.'logical-interface'
        $PrimaryInterface = $SubInterface.name | Select-String -Pattern "[gx]e-\d\/\d\/\d{1,2}" | ForEach-Object { $_.Matches.Value }
        $PrimaryInterface = Invoke-SSHCommand -SessionId $Router -command "show interfaces $($PrimaryInterface) | display xml" 
        $PrimaryInterface = Convert-JuniperXMLtoObject $PrimaryInterface
        $PrimaryInterface = $PrimaryInterface.Node.'interface-information'.'physical-interface'
        $RouterName = Get-RouterName -IP $($CoreConnections.Host[$Router])
        Write-Host -ForegroundColor $Colors.Info "`n-----------$Query found on $($RouterName.Name)--------------`n"
        If ($SuperInterface) {Write-Host "Super-Interface: $($SuperInterface.name) - $($SuperInterface.description)"}
        Write-Host "Physical Interface: $($PrimaryInterface.name) - $($PrimaryInterface.description)"
        Write-Host "Sub-Interface: $($SubInterface.name) - $($SubInterface.description)"
        Write-Host "IP: $($Results.'ip-address')"
        Write-Host "MAC: $($Results.'mac-address')"
        Write-Host -ForegroundColor $Colors.Info "`n--------------------------------------------------------------`n"
    }
}