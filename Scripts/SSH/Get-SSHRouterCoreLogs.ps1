Get-SSHCoreRouterConnectionStatus        
$Routers = Select-SSHRouter
[int]$NumOfLines = Read-Host -Prompt "Lines of logs do you want to retrieve (Default = 10)"
If (!$NumOfLines){$NumOfLines = 10}
ForEach ($Router in $($Routers.SessionID)){
    $RouterName = Get-RouterName -IP $($CoreConnections.Host[$Router])
    Write-Host -ForegroundColor $Colors.Info "`nGathering logs for $($RouterName.Name)..."
    $Logs = Invoke-SSHCommand -SessionID $Router -Command "show log messages | last $NumOfLines"
    Add-FullWidthLine -Color Yellow
    Write-Output $($Logs.Output) 
    Add-FullWidthLine -Color Yellow
}