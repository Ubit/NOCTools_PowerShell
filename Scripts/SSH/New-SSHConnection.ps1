param ($Server,$Login,$Name)
If (!$Server){
    Write-Host "What device are you trying to connect to:"
    $Server = Read-Host
}
$SessionAlreadyOpen = $NULL
$SessionAlreadyOpen = Get-SSHSession | Where-Object -Property Host -eq $Server
Write-Output $SessionAlreadyOpen
If ($SessionAlreadyOpen){Return $SessionAlreadyOpen}
If (!$SessionAlreadyOpen){
    If (!$Login){$Login = Get-Credential}
    Write-Host -ForegroundColor $Colors.Info "`nCreating connection to $Name...."
    Try{
        $Device = $(New-SSHSession -ComputerName $Server -Credential $Login -ErrorAction Stop | Out-Null)
        Return $Device
    }
    Catch{
        Write-Host -ForegroundColor $Colors.Warn "`nThere was an error when attempting to login. Check your credentials and connection, then try again.`n"
        Throw
        Break
    }
}