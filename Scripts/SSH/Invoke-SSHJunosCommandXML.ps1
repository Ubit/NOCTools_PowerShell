param ($Device,[boolean]$Ephemoral,$Command)
$ExistingConnection = Get-SSHSession | Where-Object -Property "Host" -eq "$Device"
If (!$ExistingConnection){
    New-SSHConnection -Server $Device
    $Device = Get-SSHSession | Where-Object -Property "Host" -eq "$Device"
}
If ($ExistingConnection){$Device = $ExistingConnection[0]}
$Output = $NULL
While (!$Output.Node){
    $Output = Invoke-SSHCommand -SessionId $Device[0].SessionID -Command "$Command | display xml" -TimeOut 10
    If ($Output){$Output = Convert-JuniperXMLtoObject -XMLin $Output}
}
If ($Ephemoral){Remove-SSHSession -SessionId $Device.SessionID}
Return ($Output)