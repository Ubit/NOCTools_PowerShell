param ($Bandwidth,$Router)
Get-SSHCoreRouterConnectionStatus
$Routers = Select-SSHRouter
If (!$Bandwidth){$Bandwidth = Read-Host -Prompt "`nSearch for interfaces using greater than what bandwidth?"}
If ($Bandwidth -notlike "*[a-zA-Z]*"){[float]$Bandwidth = $Bandwidth}
If ($Bandwidth -like "*[a-zA-Z]*"){[float]$BandwidthNum = $Bandwidth -replace "[^0-9,/.]" , ''}
If ($Bandwidth -like "*g*"){$Bandwidth = $BandwidthNum * 1000000000}
If ($Bandwidth -like "*m*"){$Bandwidth = $BandwidthNum * 1000000}
If ($Bandwidth -like "*k*"){$Bandwidth = $BandwidthNum * 1000}
ForEach ($Router in $Routers){
    $RouterName = Get-RouterName -IP $Router.Host
    Write-Host -ForegroundColor $Colors.Good "`nScanning ports for bandwidth utilization greater than $($Bandwidth.ToString("N0")) bps on $($RouterName.Name))"
    $InterfaceXML = Invoke-SSHCommand -SessionId $Router.SessionID -Command "show interfaces extensive detail | display xml"
    $Interfaces = Convert-JuniperXMLtoObject -XMLin $InterfaceXML
    ForEach ($Interface in $Interfaces.Node.'interface-information'.'physical-interface') {
        [float]$Inputbps = $($Interface.'traffic-statistics'.'input-bps')
        [float]$Outputbps = $($Interface.'traffic-statistics'.'output-bps') 
        If ($Inputbps -gt $Bandwidth -or $Outputbps -gt $Bandwidth) {
            Write-Host -ForegroundColor $Colors.Info "---"
            Write-Host "Interface: $($Interface.Name) ($($Interface.Description))`nInput bps: $($Inputbps.ToString('N0'))`nOutput bps: $($Outputbps.ToString('N0'))"
            Write-Host -ForegroundColor $Colors.Info "---"
        }
    }
}