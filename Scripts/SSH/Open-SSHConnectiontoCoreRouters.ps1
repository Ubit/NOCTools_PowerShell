$global:CoreConnections = @(Get-SSHSession)
If ($CoreConnections){
   Write-Host -ForegroundColor $Colors.Warn "Existing SSH Sessions found..." 
   $Check = Read-Host "`nDo you wish to clear them?"
    If ($Check -ieq "y"){
        Write-Host -ForegroundColor $Colors.Info "Clearing existing SSH Sessions..."
        ForEach ($Router in $CoreConnections) { Remove-SSHSession -SessionID $Router.SessionID | Out-Null } 
    }
    $CoreConnections = $NULL
    If ($CoreConnections -or $Check -like "*n*" -or !$Check){
        Write-Host -ForegroundColor $Colors.Warn "`nUnable to clear existing SSH connections"
        Break
    }
}
$global:CoreConnectionNames = @()
If (!$Login -and !$CoreConnections) {
    Write-Host -ForegroundColor $Colors.Good "`nRequesting credentials for login..."
    $Login = Get-Credential 
    $Script:Count = 0
    foreach ($Router in $CoreRouters) {
        New-SSHConnection -Server $Router.IP -Login $Login -Name $Router.Name
        $global:CoreConnections = @(Get-SSHSession)
        If ($CoreConnections.Host){
            If ($($CoreConnections[$Count].Host) -ieq $Router.IP){Write-Host -ForegroundColor $Colors.Good "`nConnection established to $($Router.Name)"}
            Else{Write-Host -ForegroundColor $Colors.Warn "`n$($Router.FQDN) was unable to connect."}
            $Count = $Count + 1
        }
    }
    foreach ($Router in $CoreConnections){
        $IP = $Router.Host
        $Name = $CoreRouters | Where-Object -Property "IP" -EQ "$($Router.Host)"
        $global:CoreConnectionNames +=[PSCustomObject] @{
            Name = "$($Name.Name)"
            IP = "$IP"
        }
    }
    Return
}
    