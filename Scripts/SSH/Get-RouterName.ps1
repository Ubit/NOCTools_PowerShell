param ($IP,$Name)
If ($IP){
    $RouterName = $global:CoreConnectionNames | Where-Object -Property "IP" -EQ $IP 
}
If ($Name){
    $RouterName = $global:CoreConnectionNames | Where-Object -Property "Name" -Match "(.*)$Name(.*)"
}
Return $RouterName