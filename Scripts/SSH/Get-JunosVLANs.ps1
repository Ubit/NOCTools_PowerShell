param ($Device)
$VLANData = $NULL
While (!$VLANData){
    Write-Host "Gathering VLAN data..."
    $VLAND = Invoke-SSHJunosCommandXML -Device $Device -Command "show vlans"
    $VLANData = $($VLAND.Node.'vlan-information'.vlan)
}
Return $VLANData