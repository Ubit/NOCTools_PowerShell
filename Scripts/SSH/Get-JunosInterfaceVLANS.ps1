param ($Device)
$InterfaceData = $NULL
While (!$InterfaceData){
    Write-Host "Gathering interface data..."
    $Int = Invoke-SSHJunosCommandXML -Device $Device -Command "show ethernet-switching interface"
    $InterfaceData = $($Int.Node.'switching-interface-information'.interface)
}
Return $InterfaceData