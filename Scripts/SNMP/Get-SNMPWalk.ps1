param ($SNMP,$Device,$SNMPString,$SNMPOID)
If (!$SNMPOID){$SNMPOID = ".1.3.6.1.2.1.1"}
If (!$SNMP) {$SNMP = New-SNMPConnection -Device $Device -SNMPString $SNMPString}
$SNMPDATA=$SNMP.GetTree(".$SNMPOID")
$RESULT=@()
for($i=0;$i-lt $SNMPDATA.length/2;$i++){
    $RESULT+=[pscustomobject]@{
    "SNMP ID"=$SNMPDATA[0,$i];
    "SNMP Value"=$SNMPDATA[1,$i];
    OID=($snmp.OIDFromString(($SNMPDATA[0,$i])) -join ".")} 
}
Return $RESULT 