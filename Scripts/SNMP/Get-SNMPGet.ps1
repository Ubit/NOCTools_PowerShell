param ($SNMP,$Device,$SNMPString,$SNMPOID)
If (!$SNMPOID){$SNMPOID = ".1.3.6.1.2.1.1.1.0"}
If (!$SNMP) {$SNMP = New-SNMPConnection -Device $Device -SNMPString $SNMPString}
$RESULT=$SNMP.Get(".$SNMPOID")
Return $RESULT 