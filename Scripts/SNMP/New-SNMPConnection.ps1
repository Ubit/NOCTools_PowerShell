param ($Device,$SNMPString)
If (!$(Get-ICMPStatus -Destination $Device)){
    Write-Host -ForegroundColor $Colors.Info "Unable to ping $Device exiting..."
    Break
}
$SNMP = New-Object -ComObject olePrn.OleSNMP
If (!$SNMPString) {$SNMPString = $SNMPStrings}
If($Device -and $SNMP -and $SNMPString){
    foreach ($SNMPAuth in $SNMPString) {
        $SNMP.Open("$Device","$SNMPAuth",2,1000)
        Try {$TestConnection = $SNMP.Get(".1.3.6.1.2.1.1.1.0")}
        Catch {}
        If ($TestConnection) {Return $SNMP}
    }
}
If (!$TestConnection){Write-Host -ForegroundColor $Colors.Warn "Unable to connect to $Device"}
Return $false